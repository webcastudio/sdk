/**
 * Webcastudio API
 * Webcastudio WMS API
 *
 * OpenAPI spec version: 4.0.0-alpha.35
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.webcastudio);
  }
}(this, function(expect, webcastudio) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new webcastudio.InteractionApi();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('InteractionApi', function() {
    describe('assignModerator', function() {
      it('should call assignModerator successfully', function(done) {
        //uncomment below and update the code to test assignModerator
        //instance.assignModerator(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('authorizeModerator', function() {
      it('should call authorizeModerator successfully', function(done) {
        //uncomment below and update the code to test authorizeModerator
        //instance.authorizeModerator(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('createAnswer', function() {
      it('should call createAnswer successfully', function(done) {
        //uncomment below and update the code to test createAnswer
        //instance.createAnswer(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('createModerationToken', function() {
      it('should call createModerationToken successfully', function(done) {
        //uncomment below and update the code to test createModerationToken
        //instance.createModerationToken(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('createQuestion', function() {
      it('should call createQuestion successfully', function(done) {
        //uncomment below and update the code to test createQuestion
        //instance.createQuestion(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('findAllQuestions', function() {
      it('should call findAllQuestions successfully', function(done) {
        //uncomment below and update the code to test findAllQuestions
        //instance.findAllQuestions(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('removeQuestions', function() {
      it('should call removeQuestions successfully', function(done) {
        //uncomment below and update the code to test removeQuestions
        //instance.removeQuestions(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('unAssignModerator', function() {
      it('should call unAssignModerator successfully', function(done) {
        //uncomment below and update the code to test unAssignModerator
        //instance.unAssignModerator(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('updateQuestion', function() {
      it('should call updateQuestion successfully', function(done) {
        //uncomment below and update the code to test updateQuestion
        //instance.updateQuestion(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
  });

}));
