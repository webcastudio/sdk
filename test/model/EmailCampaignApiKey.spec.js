/**
 * Webcastudio API
 * Webcastudio WMS API
 *
 * OpenAPI spec version: 4.0.0-beta.5
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.webcastudio);
  }
}(this, function(expect, webcastudio) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new webcastudio.EmailCampaignApiKey();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('EmailCampaignApiKey', function() {
    it('should create an instance of EmailCampaignApiKey', function() {
      // uncomment below and update the code to test EmailCampaignApiKey
      //var instane = new webcastudio.EmailCampaignApiKey();
      //expect(instance).to.be.a(webcastudio.EmailCampaignApiKey);
    });

    it('should have the property id (base name: "id")', function() {
      // uncomment below and update the code to test the property id
      //var instane = new webcastudio.EmailCampaignApiKey();
      //expect(instance).to.be();
    });

    it('should have the property createdAt (base name: "createdAt")', function() {
      // uncomment below and update the code to test the property createdAt
      //var instane = new webcastudio.EmailCampaignApiKey();
      //expect(instance).to.be();
    });

    it('should have the property updatedAt (base name: "updatedAt")', function() {
      // uncomment below and update the code to test the property updatedAt
      //var instane = new webcastudio.EmailCampaignApiKey();
      //expect(instance).to.be();
    });

    it('should have the property description (base name: "description")', function() {
      // uncomment below and update the code to test the property description
      //var instane = new webcastudio.EmailCampaignApiKey();
      //expect(instance).to.be();
    });

    it('should have the property provider (base name: "provider")', function() {
      // uncomment below and update the code to test the property provider
      //var instane = new webcastudio.EmailCampaignApiKey();
      //expect(instance).to.be();
    });

    it('should have the property key (base name: "key")', function() {
      // uncomment below and update the code to test the property key
      //var instane = new webcastudio.EmailCampaignApiKey();
      //expect(instance).to.be();
    });

    it('should have the property workspaceId (base name: "workspaceId")', function() {
      // uncomment below and update the code to test the property workspaceId
      //var instane = new webcastudio.EmailCampaignApiKey();
      //expect(instance).to.be();
    });

  });

}));
