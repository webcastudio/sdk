/**
 * Webcastudio API
 * Webcastudio WMS API
 *
 * OpenAPI spec version: 4.0.0-beta.4
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.webcastudio);
  }
}(this, function(expect, webcastudio) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new webcastudio.RegistrationFieldValueList();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('RegistrationFieldValueList', function() {
    it('should create an instance of RegistrationFieldValueList', function() {
      // uncomment below and update the code to test RegistrationFieldValueList
      //var instane = new webcastudio.RegistrationFieldValueList();
      //expect(instance).to.be.a(webcastudio.RegistrationFieldValueList);
    });

    it('should have the property totalItems (base name: "totalItems")', function() {
      // uncomment below and update the code to test the property totalItems
      //var instane = new webcastudio.RegistrationFieldValueList();
      //expect(instance).to.be();
    });

    it('should have the property isTruncated (base name: "isTruncated")', function() {
      // uncomment below and update the code to test the property isTruncated
      //var instane = new webcastudio.RegistrationFieldValueList();
      //expect(instance).to.be();
    });

    it('should have the property items (base name: "items")', function() {
      // uncomment below and update the code to test the property items
      //var instane = new webcastudio.RegistrationFieldValueList();
      //expect(instance).to.be();
    });

  });

}));
