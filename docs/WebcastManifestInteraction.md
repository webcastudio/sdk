# webcastudio.WebcastManifestInteraction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**socket** | [**WebcastManifestInteractionSocket**](WebcastManifestInteractionSocket.md) |  | [optional] 
**rest** | [**WebcastManifestInteractionRest**](WebcastManifestInteractionRest.md) |  | [optional] 


