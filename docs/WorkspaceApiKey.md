# webcastudio.WorkspaceApiKey

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**token** | **String** | Auth token to access the API. | [optional] 
**enabled** | **Boolean** | Whether the api key is enabled. | [optional] 
**description** | **String** | Short description to identify the api key. | [optional] 
**workspacePermissions** | **Number** | Permissions of this api key over its workspace. | [optional] 
**webcastEventPermissions** | **Number** | Permissions of this api key over the events of the workspace. | [optional] 
**workspaceId** | **Number** | Id of the workspace where the api key belongs. | [optional] 
**createdBy** | **Number** | Id of the user who created the api key. | [optional] 


