# webcastudio.ImageContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**name** | **String** | Image description. | [optional] 
**type** | **String** | Content type. | [optional] [default to &#39;image&#39;]
**meta** | [**ImageContentMeta**](ImageContentMeta.md) |  | [optional] 
**links** | [**[ContentLink]**](ContentLink.md) | Content links list. | [optional] 


<a name="TypeEnum"></a>
## Enum: TypeEnum


* `image` (value: `"image"`)




