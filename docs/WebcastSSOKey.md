# webcastudio.WebcastSSOKey

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**description** | **String** | Key pair description. | [optional] 
**publicKey** | **String** | Public key. | [optional] 
**secretKey** | **String** | Secret key. | [optional] 
**algo** | **String** | Signing algorithm. | [optional] [default to &#39;sha256&#39;]
**enabled** | **Boolean** | Whether the key pair is enabled. | [optional] [default to true]
**workspaceId** | **Number** | Workspace id. | [optional] 


<a name="AlgoEnum"></a>
## Enum: AlgoEnum


* `md5` (value: `"md5"`)

* `sha1` (value: `"sha1"`)

* `sha256` (value: `"sha256"`)




