# webcastudio.EndUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | End user id. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**email** | **String** | End user email. | [optional] 
**firstName** | **String** | End user first name. | [optional] 
**lastName** | **String** | End user last name. | [optional] 
**gender** | **String** | End user gender or NA if not available. | [optional] 
**honorific** | **String** | End user honorific treatment. | [optional] 
**workspaceId** | **Number** | Workspace where the user belongs. | [optional] 


<a name="GenderEnum"></a>
## Enum: GenderEnum


* `F` (value: `"F"`)

* `M` (value: `"M"`)

* `NA` (value: `"NA"`)




