# webcastudio.WebcastLinkAssetApi

All URIs are relative to *http://localhost:3000/api/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create**](WebcastLinkAssetApi.md#create) | **POST** /events/links/assets | Create WebcastLinkAsset
[**findAll**](WebcastLinkAssetApi.md#findAll) | **GET** /events/links/assets | Find WebcastLinkAsset
[**remove**](WebcastLinkAssetApi.md#remove) | **DELETE** /events/links/assets/{id} | Delete WebcastLinkAsset


<a name="create"></a>
# **create**
> WebcastLinkAsset create(body)

Create WebcastLinkAsset

Creates a new WebcastLinkAsset 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
defaultClient.credentials(process.env.ACCESS_KEY_ID, process.env.SECRET_KEY_ID);


var apiInstance = new webcastudio.WebcastLinkAssetApi();

var body = new webcastudio.WebcastLinkAsset(); // WebcastLinkAsset | 

apiInstance.create(body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**WebcastLinkAsset**](WebcastLinkAsset.md)|  | 

### Return type

[**WebcastLinkAsset**](WebcastLinkAsset.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="findAll"></a>
# **findAll**
> WebcastLinkAssetList findAll(opts)

Find WebcastLinkAsset

Find all WebcastLinkAsset matching the query.

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
defaultClient.credentials(process.env.ACCESS_KEY_ID, process.env.SECRET_KEY_ID);


var apiInstance = new webcastudio.WebcastLinkAssetApi();

var opts = { 
  'limit': 56, // Number | Limit of items per page
  'offset': 56, // Number | Items offset
  'where': "where_example" // String | Where statement
};
apiInstance.findAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Number**| Limit of items per page | [optional] 
 **offset** | **Number**| Items offset | [optional] 
 **where** | **String**| Where statement | [optional] 

### Return type

[**WebcastLinkAssetList**](WebcastLinkAssetList.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="remove"></a>
# **remove**
> InlineResponse2001 remove(id)

Delete WebcastLinkAsset

Deletes a WebcastLinkAsset 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
defaultClient.credentials(process.env.ACCESS_KEY_ID, process.env.SECRET_KEY_ID);


var apiInstance = new webcastudio.WebcastLinkAssetApi();

var id = 56; // Number | 

apiInstance.remove(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

