# webcastudio.WebcastSSOKeysUpdateBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **Boolean** | Whether the key pair is enabled. | [optional] 


