# webcastudio.AudienceInteractionApi

All URIs are relative to *https://api.vancastvideo.com/api/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**findAllAudienceQuestions**](AudienceInteractionApi.md#findAllAudienceQuestions) | **GET** /event/webcast/{sessionId}/qa/questions | Find Question


<a name="findAllAudienceQuestions"></a>
# **findAllAudienceQuestions**
> QuestionList findAllAudienceQuestions(sessionId, opts)

Find Question

Find all Question matching the query.

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.AudienceInteractionApi();

var sessionId = 56; // Number | 

var opts = { 
  'limit': 56, // Number | Limit of items per page
  'offset': 56, // Number | Items offset
  'where': "where_example" // String | Where statement
};
apiInstance.findAllAudienceQuestions(sessionId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionId** | **Number**|  | 
 **limit** | **Number**| Limit of items per page | [optional] 
 **offset** | **Number**| Items offset | [optional] 
 **where** | **String**| Where statement | [optional] 

### Return type

[**QuestionList**](QuestionList.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

