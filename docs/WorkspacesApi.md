# webcastudio.WorkspacesApi

All URIs are relative to *https://api.vancastvideo.com/api/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**changeWorkspacesMember**](WorkspacesApi.md#changeWorkspacesMember) | **PUT** /workspaces/{workspaceId}/members | Change Member permissions
[**createEmailCampaignApiKey**](WorkspacesApi.md#createEmailCampaignApiKey) | **POST** /workspaces/{workspaceId}/integrations/emailcampaigns/apikeys | Create EmailCampaignApiKey
[**createWebcastSSOKeys**](WorkspacesApi.md#createWebcastSSOKeys) | **POST** /workspaces/{workspaceId}/sso-keys | Create Webcast SSO Key pair
[**createWorkspaceApiKey**](WorkspacesApi.md#createWorkspaceApiKey) | **POST** /workspaces/{workspaceId}/apikeys | Create WorkspaceApiKey
[**createWorkspaceJoinInvitation**](WorkspacesApi.md#createWorkspaceJoinInvitation) | **GET** /workspaces/{workspaceId}/invitations | Creates workspace join invitation.
[**getEmailCampaignApiKeys**](WorkspacesApi.md#getEmailCampaignApiKeys) | **GET** /workspaces/{workspaceId}/integrations/emailcampaigns/apikeys | Find EmailCampaignApiKey
[**getWorkspaceApiKey**](WorkspacesApi.md#getWorkspaceApiKey) | **GET** /workspaces/{workspaceId}/apikeys/{id} | Get WorkspaceApiKey
[**getWorkspaceApiKeys**](WorkspacesApi.md#getWorkspaceApiKeys) | **GET** /workspaces/{workspaceId}/apikeys | Find WorkspaceApiKey
[**getWorkspaceSSOKeys**](WorkspacesApi.md#getWorkspaceSSOKeys) | **GET** /workspaces/{workspaceId}/sso-keys | Get Webcast SSO Key pairs list
[**getWorkspaceTranscodingPresets**](WorkspacesApi.md#getWorkspaceTranscodingPresets) | **GET** /workspaces/{workspaceId}/transcoding/presets | Get Workspace Transcoding Presets
[**getWorkspacesMembers**](WorkspacesApi.md#getWorkspacesMembers) | **GET** /workspaces/{workspaceId}/members | Get Members
[**removeEmailCampaignApiKey**](WorkspacesApi.md#removeEmailCampaignApiKey) | **DELETE** /workspaces/{workspaceId}/integrations/emailcampaigns/apikeys/{id} | Delete EmailCampaignApiKey
[**removeWebcastSSOKey**](WorkspacesApi.md#removeWebcastSSOKey) | **DELETE** /workspaces/{workspaceId}/sso-keys/{id} | Delete a Webcast SSO Key pair
[**removeWorkspaceApiKey**](WorkspacesApi.md#removeWorkspaceApiKey) | **DELETE** /workspaces/{workspaceId}/apikeys/{id} | Delete WorkspaceApiKey
[**removeWorkspacesMember**](WorkspacesApi.md#removeWorkspacesMember) | **DELETE** /workspaces/{workspaceId}/members | Remove Member
[**updateEmailCampaignApiKey**](WorkspacesApi.md#updateEmailCampaignApiKey) | **PUT** /workspaces/{workspaceId}/integrations/emailcampaigns/apikeys/{id} | Update EmailCampaignApiKey
[**updateWebcastSSOKeys**](WorkspacesApi.md#updateWebcastSSOKeys) | **PUT** /workspaces/{workspaceId}/sso-keys/{id} | Update a Webcast SSO Key pair
[**updateWorkspaceApiKey**](WorkspacesApi.md#updateWorkspaceApiKey) | **PUT** /workspaces/{workspaceId}/apikeys/{id} | Update WorkspaceApiKey


<a name="changeWorkspacesMember"></a>
# **changeWorkspacesMember**
> User changeWorkspacesMember(workspaceId, userId)

Change Member permissions

Change a member permissions to a workspace

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WorkspacesApi();

var workspaceId = 56; // Number | 

var userId = 56; // Number | 

apiInstance.changeWorkspacesMember(workspaceId, userId).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workspaceId** | **Number**|  | 
 **userId** | **Number**|  | 

### Return type

[**User**](User.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createEmailCampaignApiKey"></a>
# **createEmailCampaignApiKey**
> EmailCampaignApiKey createEmailCampaignApiKey(workspaceId, body)

Create EmailCampaignApiKey

Creates a new EmailCampaignApiKey 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WorkspacesApi();

var workspaceId = 56; // Number | Workspace id

var body = new webcastudio.EmailCampaignApiKey(); // EmailCampaignApiKey | 

apiInstance.createEmailCampaignApiKey(workspaceId, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workspaceId** | **Number**| Workspace id | 
 **body** | [**EmailCampaignApiKey**](EmailCampaignApiKey.md)|  | 

### Return type

[**EmailCampaignApiKey**](EmailCampaignApiKey.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createWebcastSSOKeys"></a>
# **createWebcastSSOKeys**
> WebcastSSOKey createWebcastSSOKeys(workspaceId, body)

Create Webcast SSO Key pair

Creates new Webcast SSO Key pair 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WorkspacesApi();

var workspaceId = 56; // Number | 

var body = new webcastudio.WebcastSSOKeysCreateBody(); // WebcastSSOKeysCreateBody | 

apiInstance.createWebcastSSOKeys(workspaceId, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workspaceId** | **Number**|  | 
 **body** | [**WebcastSSOKeysCreateBody**](WebcastSSOKeysCreateBody.md)|  | 

### Return type

[**WebcastSSOKey**](WebcastSSOKey.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createWorkspaceApiKey"></a>
# **createWorkspaceApiKey**
> WorkspaceApiKey createWorkspaceApiKey(workspaceId, body)

Create WorkspaceApiKey

Creates a new WorkspaceApiKey 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WorkspacesApi();

var workspaceId = 56; // Number | 

var body = new webcastudio.WorkspaceApiKey(); // WorkspaceApiKey | 

apiInstance.createWorkspaceApiKey(workspaceId, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workspaceId** | **Number**|  | 
 **body** | [**WorkspaceApiKey**](WorkspaceApiKey.md)|  | 

### Return type

[**WorkspaceApiKey**](WorkspaceApiKey.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createWorkspaceJoinInvitation"></a>
# **createWorkspaceJoinInvitation**
> WorkspaceJoinInvitation createWorkspaceJoinInvitation(workspaceId, body)

Creates workspace join invitation.

Creates a new workspace join invitation 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WorkspacesApi();

var workspaceId = 56; // Number | 

var body = new webcastudio.WorkspaceJoinInvitationCreateBody(); // WorkspaceJoinInvitationCreateBody | 

apiInstance.createWorkspaceJoinInvitation(workspaceId, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workspaceId** | **Number**|  | 
 **body** | [**WorkspaceJoinInvitationCreateBody**](WorkspaceJoinInvitationCreateBody.md)|  | 

### Return type

[**WorkspaceJoinInvitation**](WorkspaceJoinInvitation.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getEmailCampaignApiKeys"></a>
# **getEmailCampaignApiKeys**
> EmailCampaignApiKeyList getEmailCampaignApiKeys(workspaceId, opts)

Find EmailCampaignApiKey

Find all EmailCampaignApiKey matching the query.

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WorkspacesApi();

var workspaceId = 56; // Number | Workspace id

var opts = { 
  'limit': 56, // Number | Limit of items per page
  'offset': 56, // Number | Items offset
  'where': "where_example" // String | Where statement
};
apiInstance.getEmailCampaignApiKeys(workspaceId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workspaceId** | **Number**| Workspace id | 
 **limit** | **Number**| Limit of items per page | [optional] 
 **offset** | **Number**| Items offset | [optional] 
 **where** | **String**| Where statement | [optional] 

### Return type

[**EmailCampaignApiKeyList**](EmailCampaignApiKeyList.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getWorkspaceApiKey"></a>
# **getWorkspaceApiKey**
> WorkspaceApiKey getWorkspaceApiKey(workspaceId, id)

Get WorkspaceApiKey

Get the details of a WorkspaceApiKey by id. 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WorkspacesApi();

var workspaceId = 56; // Number | 

var id = 56; // Number | 

apiInstance.getWorkspaceApiKey(workspaceId, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workspaceId** | **Number**|  | 
 **id** | **Number**|  | 

### Return type

[**WorkspaceApiKey**](WorkspaceApiKey.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getWorkspaceApiKeys"></a>
# **getWorkspaceApiKeys**
> WorkspaceApiKeyList getWorkspaceApiKeys(workspaceId, opts)

Find WorkspaceApiKey

Find all WorkspaceApiKey belonging to the workspace and matching the query.

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WorkspacesApi();

var workspaceId = 56; // Number | 

var opts = { 
  'limit': 56, // Number | Limit of items per page
  'offset': 56, // Number | Items offset
  'where': "where_example" // String | Where statement
};
apiInstance.getWorkspaceApiKeys(workspaceId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workspaceId** | **Number**|  | 
 **limit** | **Number**| Limit of items per page | [optional] 
 **offset** | **Number**| Items offset | [optional] 
 **where** | **String**| Where statement | [optional] 

### Return type

[**WorkspaceApiKeyList**](WorkspaceApiKeyList.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getWorkspaceSSOKeys"></a>
# **getWorkspaceSSOKeys**
> WebcastSSOKeyList getWorkspaceSSOKeys(workspaceId, opts)

Get Webcast SSO Key pairs list

Get the list of Webcast SSO Key pairs 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WorkspacesApi();

var workspaceId = 56; // Number | 

var opts = { 
  'limit': 56, // Number | Limit of items per page
  'offset': 56, // Number | Items offset
  'where': "where_example" // String | Where statement
};
apiInstance.getWorkspaceSSOKeys(workspaceId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workspaceId** | **Number**|  | 
 **limit** | **Number**| Limit of items per page | [optional] 
 **offset** | **Number**| Items offset | [optional] 
 **where** | **String**| Where statement | [optional] 

### Return type

[**WebcastSSOKeyList**](WebcastSSOKeyList.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getWorkspaceTranscodingPresets"></a>
# **getWorkspaceTranscodingPresets**
> TranscodingPreset getWorkspaceTranscodingPresets(workspaceId)

Get Workspace Transcoding Presets

Get Workspace Transcoding Presets

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WorkspacesApi();

var workspaceId = 56; // Number | Workspace Id

apiInstance.getWorkspaceTranscodingPresets(workspaceId).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workspaceId** | **Number**| Workspace Id | 

### Return type

[**TranscodingPreset**](TranscodingPreset.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getWorkspacesMembers"></a>
# **getWorkspacesMembers**
> UserList getWorkspacesMembers(workspaceId, opts)

Get Members

Get a list of workspace members

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WorkspacesApi();

var workspaceId = 56; // Number | 

var opts = { 
  'limit': 56, // Number | Limit of items per page
  'offset': 56, // Number | Items offset
  'where': "where_example" // String | Where statement
};
apiInstance.getWorkspacesMembers(workspaceId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workspaceId** | **Number**|  | 
 **limit** | **Number**| Limit of items per page | [optional] 
 **offset** | **Number**| Items offset | [optional] 
 **where** | **String**| Where statement | [optional] 

### Return type

[**UserList**](UserList.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="removeEmailCampaignApiKey"></a>
# **removeEmailCampaignApiKey**
> InlineResponse2001 removeEmailCampaignApiKey(workspaceId, id)

Delete EmailCampaignApiKey

Deletes a EmailCampaignApiKey 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WorkspacesApi();

var workspaceId = 56; // Number | Workspace id

var id = 56; // Number | 

apiInstance.removeEmailCampaignApiKey(workspaceId, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workspaceId** | **Number**| Workspace id | 
 **id** | **Number**|  | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="removeWebcastSSOKey"></a>
# **removeWebcastSSOKey**
> InlineResponse2002 removeWebcastSSOKey(workspaceId, id)

Delete a Webcast SSO Key pair

Deletes a a Webcast SSO Key pair 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WorkspacesApi();

var workspaceId = 56; // Number | 

var id = 56; // Number | 

apiInstance.removeWebcastSSOKey(workspaceId, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workspaceId** | **Number**|  | 
 **id** | **Number**|  | 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="removeWorkspaceApiKey"></a>
# **removeWorkspaceApiKey**
> InlineResponse2001 removeWorkspaceApiKey(workspaceId, id)

Delete WorkspaceApiKey

Deletes a WorkspaceApiKey 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WorkspacesApi();

var workspaceId = 56; // Number | 

var id = 56; // Number | 

apiInstance.removeWorkspaceApiKey(workspaceId, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workspaceId** | **Number**|  | 
 **id** | **Number**|  | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="removeWorkspacesMember"></a>
# **removeWorkspacesMember**
> InlineResponse2004 removeWorkspacesMember(workspaceId, userId)

Remove Member

Remove a member from a workspace

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WorkspacesApi();

var workspaceId = 56; // Number | 

var userId = 56; // Number | 

apiInstance.removeWorkspacesMember(workspaceId, userId).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workspaceId** | **Number**|  | 
 **userId** | **Number**|  | 

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateEmailCampaignApiKey"></a>
# **updateEmailCampaignApiKey**
> EmailCampaignApiKey updateEmailCampaignApiKey(workspaceId, id, body)

Update EmailCampaignApiKey

Update a EmailCampaignApiKey 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WorkspacesApi();

var workspaceId = 56; // Number | Workspace id

var id = 56; // Number | 

var body = new webcastudio.EmailCampaignApiKey(); // EmailCampaignApiKey | 

apiInstance.updateEmailCampaignApiKey(workspaceId, id, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workspaceId** | **Number**| Workspace id | 
 **id** | **Number**|  | 
 **body** | [**EmailCampaignApiKey**](EmailCampaignApiKey.md)|  | 

### Return type

[**EmailCampaignApiKey**](EmailCampaignApiKey.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateWebcastSSOKeys"></a>
# **updateWebcastSSOKeys**
> WebcastSSOKey updateWebcastSSOKeys(workspaceId, id, body)

Update a Webcast SSO Key pair

Update a a Webcast SSO Key pair 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WorkspacesApi();

var workspaceId = 56; // Number | 

var id = 56; // Number | 

var body = new webcastudio.WebcastSSOKeysUpdateBody(); // WebcastSSOKeysUpdateBody | 

apiInstance.updateWebcastSSOKeys(workspaceId, id, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workspaceId** | **Number**|  | 
 **id** | **Number**|  | 
 **body** | [**WebcastSSOKeysUpdateBody**](WebcastSSOKeysUpdateBody.md)|  | 

### Return type

[**WebcastSSOKey**](WebcastSSOKey.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateWorkspaceApiKey"></a>
# **updateWorkspaceApiKey**
> WorkspaceApiKey updateWorkspaceApiKey(workspaceId, id, body)

Update WorkspaceApiKey

Update a WorkspaceApiKey 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WorkspacesApi();

var workspaceId = 56; // Number | 

var id = 56; // Number | 

var body = new webcastudio.WorkspaceApiKey(); // WorkspaceApiKey | 

apiInstance.updateWorkspaceApiKey(workspaceId, id, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workspaceId** | **Number**|  | 
 **id** | **Number**|  | 
 **body** | [**WorkspaceApiKey**](WorkspaceApiKey.md)|  | 

### Return type

[**WorkspaceApiKey**](WorkspaceApiKey.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

