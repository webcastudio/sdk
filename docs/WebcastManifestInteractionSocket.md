# webcastudio.WebcastManifestInteractionSocket

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **Boolean** | Whether the app should connect to Interaction service. | [optional] 
**url** | **String** | Interaction socket url. | [optional] 


