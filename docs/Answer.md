# webcastudio.Answer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**body** | **String** | [[Description]]. | [optional] 
**type** | **String** | [[Description]]. | [optional] 
**questionId** | **Number** | Question identifier. | [optional] 
**endUserId** | **Number** | Identifier of the user who created this answer. | [optional] 
**question** | [**Question**](Question.md) |  | [optional] 
**endUser** | [**EndUser**](EndUser.md) |  | [optional] 


