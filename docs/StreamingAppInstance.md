# webcastudio.StreamingAppInstance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**name** | **String** | Streaming application instance name. | [optional] 
**instanceId** | **Number** | Streaming service instance id. | [optional] 
**streamingEngineId** | **Number** | StreamingEngine Id. | [optional] 
**sessionId** | **Number** | WebcastSession Id. | [optional] 
**languageId** | **Number** | WebcastLanguage Id. | [optional] 
**primaryStatus** | **String** | Status of the primary engine running this instance. | [optional] 
**backupStatus** | **String** | Status of the secondary engine running this instance. | [optional] 
**scheduledAt** | **Date** | When the application instance is scheduled to be used. | [optional] 
**scheduledUntil** | **Date** | When the application instance is scheduled to be terminated. | [optional] 
**primaryPushUrl** | **String** | Primary server publishing url. | [optional] 
**backupPushUrl** | **String** | Secondary server publishing url. | [optional] 
**streamName** | **String** | Publishing stream token. | [optional] 
**streamingEngine** | [**StreamingEngine**](StreamingEngine.md) |  | [optional] 
**streamTargets** | [**[StreamTarget]**](StreamTarget.md) |  | [optional] 


<a name="PrimaryStatusEnum"></a>
## Enum: PrimaryStatusEnum


* `scheduled` (value: `"scheduled"`)

* `pending` (value: `"pending"`)

* `setting-up` (value: `"setting-up"`)

* `ready` (value: `"ready"`)

* `publishing` (value: `"publishing"`)

* `terminated` (value: `"terminated"`)

* `failure` (value: `"failure"`)




<a name="BackupStatusEnum"></a>
## Enum: BackupStatusEnum


* `scheduled` (value: `"scheduled"`)

* `pending` (value: `"pending"`)

* `setting-up` (value: `"setting-up"`)

* `ready` (value: `"ready"`)

* `publishing` (value: `"publishing"`)

* `terminated` (value: `"terminated"`)

* `failure` (value: `"failure"`)




