# webcastudio.SlideshowContentSource

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filename** | **String** | Filename | [optional] 
**description** | **String** | Source description | [optional] 
**src** | **String** | Source path | [optional] 
**size** | **Number** | Source file size in bytes | [optional] 
**type** | **String** | Source mime-type | [optional] 


<a name="DescriptionEnum"></a>
## Enum: DescriptionEnum


* `PDF` (value: `"PDF"`)

* `iSpring Flash` (value: `"iSpring Flash"`)




<a name="TypeEnum"></a>
## Enum: TypeEnum


* `pdf` (value: `"application/pdf"`)

* `x-shockwave-flash` (value: `"application/x-shockwave-flash"`)




