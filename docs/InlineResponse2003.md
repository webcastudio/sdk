# webcastudio.InlineResponse2003

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **String** |  | [optional] [default to &#39;ok&#39;]


<a name="MessageEnum"></a>
## Enum: MessageEnum


* `ok` (value: `"ok"`)




