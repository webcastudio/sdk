# webcastudio.WebcastEventForVodResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event** | [**WebcastEvent**](WebcastEvent.md) |  | [optional] 
**ondemand** | [**OnDemand**](OnDemand.md) |  | [optional] 
**accessLink** | [**WebcastLink**](WebcastLink.md) |  | [optional] 
**uploadPolicy** | [**MediaUploadPolicy**](MediaUploadPolicy.md) |  | [optional] 


