# webcastudio.StreamingEngine

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**engineId** | **Number** | Streaming service engine id. | [optional] 
**primaryStatus** | **String** | Status of the primary engine server. | [optional] 
**backupStatus** | **String** | Status of the secondary engine server. | [optional] 
**scheduledAt** | **Date** | When this engine will be launched. | [optional] 


<a name="PrimaryStatusEnum"></a>
## Enum: PrimaryStatusEnum


* `scheduled` (value: `"scheduled"`)

* `pending` (value: `"pending"`)

* `setting-up` (value: `"setting-up"`)

* `ready` (value: `"ready"`)

* `shutting-down` (value: `"shutting-down"`)

* `terminated` (value: `"terminated"`)

* `failure` (value: `"failure"`)




<a name="BackupStatusEnum"></a>
## Enum: BackupStatusEnum


* `scheduled` (value: `"scheduled"`)

* `pending` (value: `"pending"`)

* `setting-up` (value: `"setting-up"`)

* `ready` (value: `"ready"`)

* `shutting-down` (value: `"shutting-down"`)

* `terminated` (value: `"terminated"`)

* `failure` (value: `"failure"`)




