# webcastudio.HiveTicket

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**partnerId** | **Number** | [[Description]]. | [optional] 
**customerId** | **String** | [[Description]]. | [optional] 
**ticket** | **String** | [[Description]]. | [optional] 
**serviceUrl** | **String** | [[Description]]. | [optional] 
**reportUrl** | **String** | [[Description]]. | [optional] 
**shareToken** | **String** | [[Description]]. | [optional] 
**pluginUrl** | **String** | [[Description]]. | [optional] 
**eventMetadata** | **String** | [[Description]]. | [optional] 


