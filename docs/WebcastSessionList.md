# webcastudio.WebcastSessionList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**totalItems** | **Number** | Total number of items matching the requested query. | [optional] 
**isTruncated** | **Boolean** | Whether the response did not return all existing items matching the query. | [optional] 
**items** | [**[WebcastSession]**](WebcastSession.md) |  | [optional] 


