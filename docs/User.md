# webcastudio.User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | User identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**email** | **String** | User email. | [optional] 
**firstName** | **String** | User first name | [optional] 
**lastName** | **String** | User last name | [optional] 
**username** | **String** | User alias | [optional] 
**picture** | **String** | User&#39;s picture url | [optional] 


