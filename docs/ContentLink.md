# webcastudio.ContentLink

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**cuepointId** | **Number** | Cuepoint identifier. | [optional] 
**eventId** | **Number** | WebcastEvent id. | [optional] 
**sessionId** | **Number** | WebcastSession id. It may be 0 which means &#39;Common&#39;. | [optional] 
**languageId** | **Number** | WebcastLanguage id. It may be 0 which means &#39;Common&#39;. | [optional] 
**component** | **String** | Which component will render the content. | [optional] 


