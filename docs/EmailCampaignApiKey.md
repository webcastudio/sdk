# webcastudio.EmailCampaignApiKey

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**description** | **String** | description text. | [optional] 
**provider** | **String** | Integration provider. | [optional] 
**key** | **String** | Integration API key. | [optional] 
**workspaceId** | **Number** | Workspace id. | [optional] 


<a name="ProviderEnum"></a>
## Enum: ProviderEnum


* `sendinblue` (value: `"sendinblue"`)

* `mailchimp` (value: `"mailchimp"`)




