# webcastudio.RegistrationFieldTranslation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**caption** | **String** | [[Description]]. | [optional] 
**placeholder** | **String** | [[Description]]. | [optional] 
**options** | **String** | [[Description]]. | [optional] 


