# webcastudio.TranscodingVodOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**key** | **String** | [[Description]]. | [optional] 
**segmentDuration** | **Number** | [[Description]]. | [optional] 
**presetId** | **String** | [[Description]]. | [optional] 
**thumbnailPattern** | **String** | [[Description]]. | [optional] 


