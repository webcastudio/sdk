# webcastudio.WebcastEventForVodRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Name of the Event | [optional] 
**description** | **String** | Description of the Event | [optional] 
**fileName** | **String** | Name of the media file | [optional] 
**fileSize** | **Number** | Size of the media file in bytes | [optional] 
**fileContentType** | **String** | Content type of the media file (i.e. video/mp4) | [optional] 
**templateId** | **Number** | Webcast Template Id | [optional] 
**accessType** | **String** | Access link type | [optional] 
**language** | **String** | Language code (ISO 639-1) | [optional] 
**_date** | **Date** | Date of the Event | [optional] 
**timezone** | **String** | Timezone of the Event | [optional] [default to &#39;Etc/UTC&#39;]
**publish** | **Boolean** | Whether the VOD should be published | [optional] [default to true]


<a name="AccessTypeEnum"></a>
## Enum: AccessTypeEnum


* `open` (value: `"open"`)

* `sso` (value: `"sso"`)

* `registration_gate` (value: `"registration_gate"`)

* `sso_email_campaign` (value: `"sso_email_campaign"`)




