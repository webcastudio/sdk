# webcastudio.SlideshowContentMeta

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sources** | [**SlideshowContentMetaSources**](SlideshowContentMetaSources.md) |  | [optional] 


