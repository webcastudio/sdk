# webcastudio.ChangeWebcastEventMemberRoleRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**aclRoleId** | **Number** | WebcastEvent Role id | [optional] 


