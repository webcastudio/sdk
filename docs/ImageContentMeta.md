# webcastudio.ImageContentMeta

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**renditions** | **Object** | Image renditions. | [optional] 


