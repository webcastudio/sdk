# webcastudio.WebcastAuthToken

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**token** | **String** | Access token | [optional] 
**expiresAt** | **Date** | When the token expires. | [optional] 


