# webcastudio.RegistrationFieldList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**totalItems** | **Number** | Total number of items matching the query. | [optional] 
**isTruncated** | **Boolean** | Whether there are more records matching the query that were not returned. | [optional] 
**items** | [**[RegistrationField]**](RegistrationField.md) |  | [optional] 


