# webcastudio.EventApi

All URIs are relative to *https://api.vancastvideo.com/api/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**authorizeEndUser**](EventApi.md#authorizeEndUser) | **POST** /event/webcast/{sessionId}/authorize | Authorize End User
[**endUserSignUp**](EventApi.md#endUserSignUp) | **POST** /event/signup | EndUser Signup
[**getLinkByToken**](EventApi.md#getLinkByToken) | **GET** /event/link/{token} | Get WebcastLink details by token
[**getManifest**](EventApi.md#getManifest) | **GET** /event/webcast/{sessionId}/manifest | Get WebcastManifest details
[**getStreams**](EventApi.md#getStreams) | **GET** /event/webcast/{sessionId}/streams | Get Webcast stream endpoints resource list


<a name="authorizeEndUser"></a>
# **authorizeEndUser**
> WebcastAuthToken authorizeEndUser(sessionId, body)

Authorize End User

Check whether an End User token is valid and it is authorized to access the requested Webcast Session.

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.EventApi();

var sessionId = 56; // Number | 

var body = new webcastudio.AuthorizeEndUserRequest(); // AuthorizeEndUserRequest | 

apiInstance.authorizeEndUser(sessionId, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionId** | **Number**|  | 
 **body** | [**AuthorizeEndUserRequest**](AuthorizeEndUserRequest.md)|  | 

### Return type

[**WebcastAuthToken**](WebcastAuthToken.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="endUserSignUp"></a>
# **endUserSignUp**
> EndUser endUserSignUp()

EndUser Signup

Returns the created end user

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.EventApi();
apiInstance.endUserSignUp().then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters
This endpoint does not need any parameter.

### Return type

[**EndUser**](EndUser.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getLinkByToken"></a>
# **getLinkByToken**
> WebcastLink getLinkByToken(token)

Get WebcastLink details by token

Returns WebcastLink details 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.EventApi();

var token = "token_example"; // String | 

apiInstance.getLinkByToken(token).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **String**|  | 

### Return type

[**WebcastLink**](WebcastLink.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getManifest"></a>
# **getManifest**
> WebcastManifest getManifest(sessionId)

Get WebcastManifest details

Returns WebcastManifest details 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.EventApi();

var sessionId = 56; // Number | 

apiInstance.getManifest(sessionId).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionId** | **Number**|  | 

### Return type

[**WebcastManifest**](WebcastManifest.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getStreams"></a>
# **getStreams**
> WebcastStreamsResourceList getStreams(sessionId, opts)

Get Webcast stream endpoints resource list

Return a resource list of stream endpoints. 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.EventApi();

var sessionId = 56; // Number | WebcastSession id.

var opts = { 
  'languageId': 56 // Number | WebcastLanguage Id.
};
apiInstance.getStreams(sessionId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionId** | **Number**| WebcastSession id. | 
 **languageId** | **Number**| WebcastLanguage Id. | [optional] 

### Return type

[**WebcastStreamsResourceList**](WebcastStreamsResourceList.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

