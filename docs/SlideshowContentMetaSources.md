# webcastudio.SlideshowContentMetaSources

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pdf** | [**SlideshowContentSource**](SlideshowContentSource.md) | PDF source path. | [optional] 
**ispringFlash** | [**SlideshowContentSource**](SlideshowContentSource.md) | iSpring flash source path | [optional] 


