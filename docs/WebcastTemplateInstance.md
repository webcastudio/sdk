# webcastudio.WebcastTemplateInstance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**manifest** | **Object** | Manifest settings. | [optional] 
**path** | **String** | Assets path. | [optional] 
**playerVersion** | **String** | Player version required. | [optional] 


