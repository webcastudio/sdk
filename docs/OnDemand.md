# webcastudio.OnDemand

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**published** | **Boolean** | Whether the ondemand is published. | [optional] 
**publishedUntil** | **Date** | When the ondemand will be automatically unpublished | [optional] 
**cuepoints** | **[Object]** | List of sync cues | [optional] 
**clipFolder** | **String** | Folder where the clip files are stored in the cloud storage | [optional] 
**clipStart** | **Number** | Clip start time offset in seconds | [optional] 
**clipDuration** | **Number** | Clip duration in seconds | [optional] 
**clipPlaybackUrl** | **String** | Clip playback url | [optional] 
**media** | [**Media**](Media.md) |  | [optional] 


