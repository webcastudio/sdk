# webcastudio.ContentMeta

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**type** | **String** | Metadata type | [optional] 
**value** | **String** | JSON serialized metadata object. | [optional] 


