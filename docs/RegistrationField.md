# webcastudio.RegistrationField

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**key** | **String** | Registration field unique key. This key must be unique for each webcast link. | [optional] 
**type** | **String** | Field input type. | [optional] 
**controlType** | **String** | Field control type. | [optional] 
**defaultCaption** | **String** | Default caption used when there is no translation. | [optional] 
**defaultPlaceholder** | **String** | Default placeholder used when there is no translation. | [optional] 
**defaultOptions** | **String** | Default options used when there is no translation. | [optional] 
**defaultValue** | **String** | Default value assigned to the field input. | [optional] 
**weight** | **Number** | Field weight, used to order registration fields. | [optional] 
**enabled** | **Boolean** | Whether this field will be presented to the end user or not. | [optional] 
**required** | **Boolean** | Whether this field will be required for the end user registration. | [optional] 
**_protected** | **Boolean** | Whether this field can be deleted by WMS users. | [optional] 
**necessary** | **Boolean** | Whether this field is necessary for the registration. Necessary fields cannot be disabled. | [optional] 
**mergeTag** | **String** | Tag used for value replacements in emails. | [optional] 


<a name="TypeEnum"></a>
## Enum: TypeEnum


* `text` (value: `"text"`)

* `email` (value: `"email"`)

* `number` (value: `"number"`)

* `boolean` (value: `"boolean"`)

* `date` (value: `"date"`)

* `list` (value: `"list"`)




<a name="ControlTypeEnum"></a>
## Enum: ControlTypeEnum


* `textbox` (value: `"textbox"`)

* `radio` (value: `"radio"`)

* `select` (value: `"select"`)

* `date` (value: `"date"`)




