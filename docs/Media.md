# webcastudio.Media

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**originalFilename** | **String** | Original name of the media file | [optional] 
**downloadUrl** | **String** | Download URL of the original media file | [optional] 
**path** | **String** | Relative path of the media file in the cloud storage | [optional] 
**status** | **String** | Status of the media file | [optional] 
**serverSideRecorded** | **Boolean** | Whether the media file was generated as a result of a live recoding | [optional] 
**size** | **Number** | Size of the original media file in bytes | [optional] 
**transcodingPresetId** | **Number** | Transcoding preset applied | [optional] 


<a name="StatusEnum"></a>
## Enum: StatusEnum


* `uploading` (value: `"uploading"`)

* `uploaded` (value: `"uploaded"`)

* `transcoding` (value: `"transcoding"`)

* `ready` (value: `"ready"`)




