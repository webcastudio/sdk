# webcastudio.WebcastEventAudienceAccessReport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**totalRows** | **Number** | Total number of rows in the report. | [optional] 
**isTruncated** | **Boolean** | Whether the report contains more rows than the returned. | [optional] 
**rows** | [**[WebcastEventAudienceAccessReportRow]**](WebcastEventAudienceAccessReportRow.md) |  | [optional] 


