# webcastudio.WebcastSSOKeysCreateBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **String** | Key pair description. | [optional] 
**algo** | **String** | Signing algorithm | [optional] [default to &#39;sha256&#39;]
**enabled** | **Boolean** | Whether the key pair is enabled. | [optional] [default to true]


<a name="AlgoEnum"></a>
## Enum: AlgoEnum


* `md5` (value: `"md5"`)

* `sha1` (value: `"sha1"`)

* `sha256` (value: `"sha256"`)




