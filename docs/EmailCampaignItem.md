# webcastudio.EmailCampaignItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**description** | **String** | Email campaign description text. | [optional] 
**provider** | **String** | Integration provider. | [optional] 
**attributesMap** | **Object** | Integration attribute names mapping. | [optional] 
**webcastLinkId** | **Number** | WebcastLink id. | [optional] 
**apiKeyId** | **Number** | EmailCampaignApiKey id. | [optional] 
**apiKey** | [**EmailCampaignApiKey**](EmailCampaignApiKey.md) |  | [optional] 
**psk** | **String** | Integration psk. | [optional] 


<a name="ProviderEnum"></a>
## Enum: ProviderEnum


* `sendinblue` (value: `"sendinblue"`)

* `mailchimp` (value: `"mailchimp"`)




