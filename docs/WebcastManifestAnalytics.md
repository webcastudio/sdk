# webcastudio.WebcastManifestAnalytics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reportInterval** | **Number** | Report activity interval. | [optional] 


