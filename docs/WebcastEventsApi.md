# webcastudio.WebcastEventsApi

All URIs are relative to *https://api.vancastvideo.com/api/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addWebcastEventMember**](WebcastEventsApi.md#addWebcastEventMember) | **POST** /events/{eventId}/members | Add a new member
[**changeWebcastEventMemberRole**](WebcastEventsApi.md#changeWebcastEventMemberRole) | **PUT** /events/{eventId}/members/{userId} | Change WebcastEvent member role
[**copySessionAsVod**](WebcastEventsApi.md#copySessionAsVod) | **POST** /events/{eventId}/sessions/{id}/copyasvod | Creates a new VOD session copying an existing session that was produced live.
[**createEmailCampaign**](WebcastEventsApi.md#createEmailCampaign) | **POST** /events/{eventId}/links/{linkId}/integrations/emailcampaigns | Create EmailCampaign
[**createEventForVod**](WebcastEventsApi.md#createEventForVod) | **POST** /events/vod | Creates an Event configured for VOD
[**createLink**](WebcastEventsApi.md#createLink) | **POST** /events/{eventId}/links | Create WebcastLink
[**createOnDemand**](WebcastEventsApi.md#createOnDemand) | **POST** /events/{eventId}/ondemands | Create OnDemand
[**createRegistrationField**](WebcastEventsApi.md#createRegistrationField) | **POST** /events/{eventId}/links/{linkId}/fields | Create RegistrationField
[**createRegistrationFieldTranslation**](WebcastEventsApi.md#createRegistrationFieldTranslation) | **POST** /events/{eventId}/links/{linkId}/fields/{fieldId}/translations | Create RegistrationFieldTranslation
[**createWebcastLinkAsset**](WebcastEventsApi.md#createWebcastLinkAsset) | **POST** /events/{eventId}/links/{linkId}/assets | Create WebcastLinkAsset
[**findAllWebcastLinkAssets**](WebcastEventsApi.md#findAllWebcastLinkAssets) | **GET** /events/{eventId}/links/{linkId}/assets | Find WebcastLinkAsset
[**findOnDemands**](WebcastEventsApi.md#findOnDemands) | **GET** /events/{eventId}/ondemands | Find OnDemands
[**getAudienceAccessReport**](WebcastEventsApi.md#getAudienceAccessReport) | **GET** /events/{eventId}/reports/audience/access | Audience Access
[**getAudienceViewingReport**](WebcastEventsApi.md#getAudienceViewingReport) | **GET** /events/{eventId}/reports/audience/webcast-viewing | Audience Viewing
[**getContents**](WebcastEventsApi.md#getContents) | **GET** /events/{eventId}/contents | Get Content list
[**getEmailCampaign**](WebcastEventsApi.md#getEmailCampaign) | **GET** /events/{eventId}/links/{linkId}/integrations/emailcampaigns/{id} | Find EmailCampaign
[**getEmailCampaigns**](WebcastEventsApi.md#getEmailCampaigns) | **GET** /events/{eventId}/links/{linkId}/integrations/emailcampaigns | Find EmailCampaign
[**getLink**](WebcastEventsApi.md#getLink) | **GET** /events/{eventId}/links/{id} | Get WebcastLink details
[**getLinks**](WebcastEventsApi.md#getLinks) | **GET** /events/{eventId}/links | Get WebcastLinks
[**getOndemand**](WebcastEventsApi.md#getOndemand) | **GET** /events/{eventId}/ondemands/{id} | Get OnDemand
[**getRegistrationFields**](WebcastEventsApi.md#getRegistrationFields) | **GET** /events/{eventId}/links/{linkId}/fields | Get RegistrationFields
[**getTemplateFile**](WebcastEventsApi.md#getTemplateFile) | **GET** /events/{eventId}/template/{file} | Get a template instance file
[**getWebcastEvent**](WebcastEventsApi.md#getWebcastEvent) | **GET** /events/{id} | Get WebcastWebcast details
[**getWebcastEventMembers**](WebcastEventsApi.md#getWebcastEventMembers) | **GET** /events/{eventId}/members | Get Members
[**removeContent**](WebcastEventsApi.md#removeContent) | **DELETE** /events/{eventId}/contents/{id} | Delete Content
[**removeEmailCampaign**](WebcastEventsApi.md#removeEmailCampaign) | **DELETE** /events/{eventId}/links/{linkId}/integrations/emailcampaigns/{id} | Delete EmailCampaign
[**removeImageRendition**](WebcastEventsApi.md#removeImageRendition) | **DELETE** /events/{eventId}/contents/{id}/renditions/{alias} | Delete ImageContent rendition
[**removeLink**](WebcastEventsApi.md#removeLink) | **DELETE** /events/{eventId}/links/{id} | Delete WebcastLink
[**removeOnDemand**](WebcastEventsApi.md#removeOnDemand) | **DELETE** /events/{eventId}/ondemands/{id} | Remove OnDemand
[**removeRegistrationField**](WebcastEventsApi.md#removeRegistrationField) | **DELETE** /events/{eventId}/links/{linkId}/fields/{id} | Delete RegistrationField
[**removeRegistrationFieldTranslation**](WebcastEventsApi.md#removeRegistrationFieldTranslation) | **DELETE** /events/{eventId}/links/{linkId}/fields/{fieldId}/translations/{id} | Delete RegistrationFieldTranslation
[**removeSlideshowSource**](WebcastEventsApi.md#removeSlideshowSource) | **DELETE** /events/{eventId}/contents/{id}/slideshows/sources/{alias} | Delete SlideshowContent rendition
[**removeWebcastAssetRendition**](WebcastEventsApi.md#removeWebcastAssetRendition) | **POST** /events/{eventId}/links/{linkId}/assets/{id}/renditions/{renditionName} | Deletes a WebcastLinkAsset rendition
[**removeWebcastLinkAsset**](WebcastEventsApi.md#removeWebcastLinkAsset) | **DELETE** /events/{eventId}/links/{linkId}/assets/{id} | Remove WebcastLinkAsset
[**replaceImage**](WebcastEventsApi.md#replaceImage) | **POST** /events/{eventId}/contents/{id} | Replace ImageContent
[**replaceWebcastAsset**](WebcastEventsApi.md#replaceWebcastAsset) | **POST** /events/{eventId}/links/{linkId}/assets/{id} | Replaces a WebcastLinkAsset
[**revokeWebcastEventMember**](WebcastEventsApi.md#revokeWebcastEventMember) | **DELETE** /events/{eventId}/members/{userId} | Revoke WebcastEvent member privileges
[**terminateStreamingEngine**](WebcastEventsApi.md#terminateStreamingEngine) | **DELETE** /events/{eventId}/streamingengines/{id} | Terminate an Streaming Engine
[**updateEmailCampaign**](WebcastEventsApi.md#updateEmailCampaign) | **PUT** /events/{eventId}/links/{linkId}/integrations/emailcampaigns/{id} | Update EmailCampaign
[**updateLink**](WebcastEventsApi.md#updateLink) | **PUT** /events/{eventId}/links/{id} | Update WebcastLink
[**updateLinkSettings**](WebcastEventsApi.md#updateLinkSettings) | **PUT** /events/{eventId}/links/{id}/settings | Update WebcastLink settings
[**updateLinkSettingsLevel1**](WebcastEventsApi.md#updateLinkSettingsLevel1) | **PUT** /events/{eventId}/links/{id}/settings/{key1} | Update WebcastLink settings
[**updateLinkSettingsLevel2**](WebcastEventsApi.md#updateLinkSettingsLevel2) | **PUT** /events/{eventId}/links/{id}/settings/{key1}/{key2} | Update WebcastLink settings
[**updateLinkSettingsLevel3**](WebcastEventsApi.md#updateLinkSettingsLevel3) | **PUT** /events/{eventId}/links/{id}/settings/{key1}/{key2}/{key3} | Update WebcastLink settings
[**updateOnDemand**](WebcastEventsApi.md#updateOnDemand) | **PUT** /events/{eventId}/ondemands/{id} | Update OnDemand
[**updateRegistrationField**](WebcastEventsApi.md#updateRegistrationField) | **PUT** /events/{eventId}/links/{linkId}/fields/{id} | Update RegistrationField
[**updateRegistrationFieldTranslation**](WebcastEventsApi.md#updateRegistrationFieldTranslation) | **PUT** /events/{eventId}/links/{linkId}/fields/{fieldId}/translations/{id} | Update RegistrationFieldTranslation
[**updateTemplate**](WebcastEventsApi.md#updateTemplate) | **GET** /events/{eventId}/template/ | Update template instance files
[**uploadImageRendition**](WebcastEventsApi.md#uploadImageRendition) | **POST** /events/{eventId}/contents/{id}/renditions/{alias} | Upload ImageContent rendition
[**uploadOndemandCuepoints**](WebcastEventsApi.md#uploadOndemandCuepoints) | **POST** /events/{eventId}/ondemands/{id}/cuepoints.json | Replace current cuepoints
[**uploadSlideshow**](WebcastEventsApi.md#uploadSlideshow) | **POST** /events/{eventId}/contents | Upload SlideshowContent
[**uploadSlideshowSource**](WebcastEventsApi.md#uploadSlideshowSource) | **POST** /events/{eventId}/contents/{id}/slideshows/sources/{alias} | Upload SlideshowContent source


<a name="addWebcastEventMember"></a>
# **addWebcastEventMember**
> User addWebcastEventMember(eventId, body)

Add a new member

Add a new member

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | 

var body = new webcastudio.AddWebcastEventMemberRequest(); // AddWebcastEventMemberRequest | 

apiInstance.addWebcastEventMember(eventId, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**|  | 
 **body** | [**AddWebcastEventMemberRequest**](AddWebcastEventMemberRequest.md)|  | 

### Return type

[**User**](User.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="changeWebcastEventMemberRole"></a>
# **changeWebcastEventMemberRole**
> User changeWebcastEventMemberRole(eventId, userId, body)

Change WebcastEvent member role

Change WebcastEvent member role

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | 

var userId = 56; // Number | 

var body = new webcastudio.ChangeWebcastEventMemberRoleRequest(); // ChangeWebcastEventMemberRoleRequest | 

apiInstance.changeWebcastEventMemberRole(eventId, userId, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**|  | 
 **userId** | **Number**|  | 
 **body** | [**ChangeWebcastEventMemberRoleRequest**](ChangeWebcastEventMemberRoleRequest.md)|  | 

### Return type

[**User**](User.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="copySessionAsVod"></a>
# **copySessionAsVod**
> WebcastSession copySessionAsVod(eventId, id, body)

Creates a new VOD session copying an existing session that was produced live.

Creates a new VOD session copying an existing session that was produced live.

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | WebcastEvent id

var id = 56; // Number | WebcastSession id

var body = new webcastudio.CopySessionAsVodRequest(); // CopySessionAsVodRequest | 

apiInstance.copySessionAsVod(eventId, id, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| WebcastEvent id | 
 **id** | **Number**| WebcastSession id | 
 **body** | [**CopySessionAsVodRequest**](CopySessionAsVodRequest.md)|  | 

### Return type

[**WebcastSession**](WebcastSession.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createEmailCampaign"></a>
# **createEmailCampaign**
> EmailCampaign createEmailCampaign(eventId, linkId, body)

Create EmailCampaign

Creates a new EmailCampaign 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | Webcast event id

var linkId = 56; // Number | WebcastLink id

var body = new webcastudio.EmailCampaign(); // EmailCampaign | 

apiInstance.createEmailCampaign(eventId, linkId, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| Webcast event id | 
 **linkId** | **Number**| WebcastLink id | 
 **body** | [**EmailCampaign**](EmailCampaign.md)|  | 

### Return type

[**EmailCampaign**](EmailCampaign.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createEventForVod"></a>
# **createEventForVod**
> WebcastEventForVodResponse createEventForVod(body, opts)

Creates an Event configured for VOD

Creates an Event configured for VOD

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var body = new webcastudio.WebcastEventForVodRequest(); // WebcastEventForVodRequest | 

var opts = { 
  'workspaceId': 56 // Number | Workspace Id.
};
apiInstance.createEventForVod(body, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**WebcastEventForVodRequest**](WebcastEventForVodRequest.md)|  | 
 **workspaceId** | **Number**| Workspace Id. | [optional] 

### Return type

[**WebcastEventForVodResponse**](WebcastEventForVodResponse.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createLink"></a>
# **createLink**
> WebcastLinkItem createLink(eventId, body)

Create WebcastLink

Creates new WebcastLink 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | 

var body = new webcastudio.WebcastLinkItem(); // WebcastLinkItem | 

apiInstance.createLink(eventId, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**|  | 
 **body** | [**WebcastLinkItem**](WebcastLinkItem.md)|  | 

### Return type

[**WebcastLinkItem**](WebcastLinkItem.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createOnDemand"></a>
# **createOnDemand**
> OnDemand createOnDemand(eventId, body)

Create OnDemand

Create a new OnDemand

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | WebcastEvent id

var body = new webcastudio.OnDemand(); // OnDemand | New OnDemand resource

apiInstance.createOnDemand(eventId, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| WebcastEvent id | 
 **body** | [**OnDemand**](OnDemand.md)| New OnDemand resource | 

### Return type

[**OnDemand**](OnDemand.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createRegistrationField"></a>
# **createRegistrationField**
> RegistrationField createRegistrationField(eventId, linkId, body)

Create RegistrationField

Creates new RegistrationField 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | 

var linkId = 56; // Number | 

var body = new webcastudio.RegistrationField(); // RegistrationField | 

apiInstance.createRegistrationField(eventId, linkId, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**|  | 
 **linkId** | **Number**|  | 
 **body** | [**RegistrationField**](RegistrationField.md)|  | 

### Return type

[**RegistrationField**](RegistrationField.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createRegistrationFieldTranslation"></a>
# **createRegistrationFieldTranslation**
> RegistrationFieldTranslation createRegistrationFieldTranslation(eventId, linkId, fieldId, body)

Create RegistrationFieldTranslation

Creates new RegistrationFieldTranslation 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | 

var linkId = 56; // Number | 

var fieldId = 56; // Number | 

var body = new webcastudio.RegistrationFieldTranslation(); // RegistrationFieldTranslation | 

apiInstance.createRegistrationFieldTranslation(eventId, linkId, fieldId, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**|  | 
 **linkId** | **Number**|  | 
 **fieldId** | **Number**|  | 
 **body** | [**RegistrationFieldTranslation**](RegistrationFieldTranslation.md)|  | 

### Return type

[**RegistrationFieldTranslation**](RegistrationFieldTranslation.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createWebcastLinkAsset"></a>
# **createWebcastLinkAsset**
> WebcastLinkAsset createWebcastLinkAsset(eventId, linkId, body)

Create WebcastLinkAsset

Creates a new WebcastLinkAsset 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | Webcast Event id.

var linkId = 56; // Number | Webcast Link id.

var body = new webcastudio.WebcastLinkAsset(); // WebcastLinkAsset | 

apiInstance.createWebcastLinkAsset(eventId, linkId, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| Webcast Event id. | 
 **linkId** | **Number**| Webcast Link id. | 
 **body** | [**WebcastLinkAsset**](WebcastLinkAsset.md)|  | 

### Return type

[**WebcastLinkAsset**](WebcastLinkAsset.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="findAllWebcastLinkAssets"></a>
# **findAllWebcastLinkAssets**
> WebcastLinkAssetList findAllWebcastLinkAssets(eventId, linkId, opts)

Find WebcastLinkAsset

Find all WebcastLinkAsset matching the query.

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | Webcast Event id.

var linkId = 56; // Number | Webcast Link id.

var opts = { 
  'limit': 56, // Number | Limit of items per page
  'offset': 56, // Number | Items offset
  'where': "where_example" // String | Where statement
};
apiInstance.findAllWebcastLinkAssets(eventId, linkId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| Webcast Event id. | 
 **linkId** | **Number**| Webcast Link id. | 
 **limit** | **Number**| Limit of items per page | [optional] 
 **offset** | **Number**| Items offset | [optional] 
 **where** | **String**| Where statement | [optional] 

### Return type

[**WebcastLinkAssetList**](WebcastLinkAssetList.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="findOnDemands"></a>
# **findOnDemands**
> OnDemandList findOnDemands(eventId, opts)

Find OnDemands

Find OnDemand resources 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | WebcastEvent id.

var opts = { 
  'limit': 56, // Number | Limit of items per page
  'offset': 56, // Number | Items offset
  'where': "where_example" // String | Where statement
};
apiInstance.findOnDemands(eventId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| WebcastEvent id. | 
 **limit** | **Number**| Limit of items per page | [optional] 
 **offset** | **Number**| Items offset | [optional] 
 **where** | **String**| Where statement | [optional] 

### Return type

[**OnDemandList**](OnDemandList.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getAudienceAccessReport"></a>
# **getAudienceAccessReport**
> WebcastEventAudienceAccessReport getAudienceAccessReport(eventId, opts)

Audience Access

Get Webcast Event audience access report

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | Webcast Event id

var opts = { 
  'limit': 56, // Number | Limit of items per page
  'offset': 56, // Number | Items offset
  'where': "where_example" // String | Where statement
};
apiInstance.getAudienceAccessReport(eventId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| Webcast Event id | 
 **limit** | **Number**| Limit of items per page | [optional] 
 **offset** | **Number**| Items offset | [optional] 
 **where** | **String**| Where statement | [optional] 

### Return type

[**WebcastEventAudienceAccessReport**](WebcastEventAudienceAccessReport.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getAudienceViewingReport"></a>
# **getAudienceViewingReport**
> WebcastEventAudienceViewingReport getAudienceViewingReport(eventId, opts)

Audience Viewing

Get Webcast Event audience access report

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | Webcast Event id

var opts = { 
  'limit': 56, // Number | Limit of items per page
  'offset': 56, // Number | Items offset
  'where': "where_example", // String | Where statement
  'groupByUid': true, // Boolean | Group unique users sessions.
  'removeIds': true // Boolean | Remove tables ids.
};
apiInstance.getAudienceViewingReport(eventId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| Webcast Event id | 
 **limit** | **Number**| Limit of items per page | [optional] 
 **offset** | **Number**| Items offset | [optional] 
 **where** | **String**| Where statement | [optional] 
 **groupByUid** | **Boolean**| Group unique users sessions. | [optional] 
 **removeIds** | **Boolean**| Remove tables ids. | [optional] 

### Return type

[**WebcastEventAudienceViewingReport**](WebcastEventAudienceViewingReport.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getContents"></a>
# **getContents**
> ContentList getContents(eventId, opts)

Get Content list

Find event&#39;s content list 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | WebcastEvent id.

var opts = { 
  'limit': 56, // Number | Limit of items per page
  'offset': 56, // Number | Items offset
  'where': "where_example" // String | Where statement
};
apiInstance.getContents(eventId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| WebcastEvent id. | 
 **limit** | **Number**| Limit of items per page | [optional] 
 **offset** | **Number**| Items offset | [optional] 
 **where** | **String**| Where statement | [optional] 

### Return type

[**ContentList**](ContentList.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getEmailCampaign"></a>
# **getEmailCampaign**
> EmailCampaign getEmailCampaign(eventId, linkId, id)

Find EmailCampaign

Find all EmailCampaign matching the query.

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | Webcast event id

var linkId = 56; // Number | WebcastLink id

var id = 56; // Number | 

apiInstance.getEmailCampaign(eventId, linkId, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| Webcast event id | 
 **linkId** | **Number**| WebcastLink id | 
 **id** | **Number**|  | 

### Return type

[**EmailCampaign**](EmailCampaign.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getEmailCampaigns"></a>
# **getEmailCampaigns**
> EmailCampaignList getEmailCampaigns(eventId, linkId, opts)

Find EmailCampaign

Find all EmailCampaign matching the query.

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | Webcast event id

var linkId = 56; // Number | WebcastLink id

var opts = { 
  'limit': 56, // Number | Limit of items per page
  'offset': 56, // Number | Items offset
  'where': "where_example" // String | Where statement
};
apiInstance.getEmailCampaigns(eventId, linkId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| Webcast event id | 
 **linkId** | **Number**| WebcastLink id | 
 **limit** | **Number**| Limit of items per page | [optional] 
 **offset** | **Number**| Items offset | [optional] 
 **where** | **String**| Where statement | [optional] 

### Return type

[**EmailCampaignList**](EmailCampaignList.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getLink"></a>
# **getLink**
> WebcastLink getLink(eventId, id)

Get WebcastLink details

Returns WebcastLink details 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | 

var id = 56; // Number | 

apiInstance.getLink(eventId, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**|  | 
 **id** | **Number**|  | 

### Return type

[**WebcastLink**](WebcastLink.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getLinks"></a>
# **getLinks**
> WebcastLinkList getLinks(eventId, opts)

Get WebcastLinks

Returns event&#39;s WebcastLinks 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | 

var opts = { 
  'limit': 56, // Number | Limit of items per page
  'offset': 56, // Number | Items offset
  'where': "where_example" // String | Where statement
};
apiInstance.getLinks(eventId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**|  | 
 **limit** | **Number**| Limit of items per page | [optional] 
 **offset** | **Number**| Items offset | [optional] 
 **where** | **String**| Where statement | [optional] 

### Return type

[**WebcastLinkList**](WebcastLinkList.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getOndemand"></a>
# **getOndemand**
> OnDemand getOndemand(eventId, id)

Get OnDemand

Get OnDemand resource details

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | WebcastEvent id. 

var id = 56; // Number | OnDemand id.

apiInstance.getOndemand(eventId, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| WebcastEvent id.  | 
 **id** | **Number**| OnDemand id. | 

### Return type

[**OnDemand**](OnDemand.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getRegistrationFields"></a>
# **getRegistrationFields**
> RegistrationFieldList getRegistrationFields(eventId, linkId)

Get RegistrationFields

Returns webcastlink&#39;s RegistrationFields 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | 

var linkId = 56; // Number | 

apiInstance.getRegistrationFields(eventId, linkId).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**|  | 
 **linkId** | **Number**|  | 

### Return type

[**RegistrationFieldList**](RegistrationFieldList.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getTemplateFile"></a>
# **getTemplateFile**
> &#39;String&#39; getTemplateFile(eventId, file)

Get a template instance file

Returns one of the WebcastTemplateInstance&#39;s file 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | 

var file = "file_example"; // String | 

apiInstance.getTemplateFile(eventId, file).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**|  | 
 **file** | **String**|  | 

### Return type

**&#39;String&#39;**

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getWebcastEvent"></a>
# **getWebcastEvent**
> WebcastEvent getWebcastEvent(id)

Get WebcastWebcast details

Returns Webcast Event details 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var id = 56; // Number | 

apiInstance.getWebcastEvent(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

[**WebcastEvent**](WebcastEvent.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getWebcastEventMembers"></a>
# **getWebcastEventMembers**
> UserList getWebcastEventMembers(eventId, opts)

Get Members

Returns the WebcastEvent members

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | 

var opts = { 
  'limit': 56, // Number | Limit of items per page
  'offset': 56, // Number | Items offset
  'where': "where_example", // String | Where statement
  'order': "order_example" // String | Order by statement
};
apiInstance.getWebcastEventMembers(eventId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**|  | 
 **limit** | **Number**| Limit of items per page | [optional] 
 **offset** | **Number**| Items offset | [optional] 
 **where** | **String**| Where statement | [optional] 
 **order** | **String**| Order by statement | [optional] 

### Return type

[**UserList**](UserList.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="removeContent"></a>
# **removeContent**
> InlineResponse200 removeContent(id, eventId, component, sessionId, languageId)

Delete Content

Deletes a Content 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var id = 56; // Number | 

var eventId = 56; // Number | 

var component = "component_example"; // String | 

var sessionId = 56; // Number | 

var languageId = 56; // Number | 

apiInstance.removeContent(id, eventId, component, sessionId, languageId).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **eventId** | **Number**|  | 
 **component** | **String**|  | 
 **sessionId** | **Number**|  | 
 **languageId** | **Number**|  | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="removeEmailCampaign"></a>
# **removeEmailCampaign**
> InlineResponse2001 removeEmailCampaign(eventId, linkId, id)

Delete EmailCampaign

Deletes a EmailCampaign 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | Webcast event id

var linkId = 56; // Number | WebcastLink id

var id = 56; // Number | 

apiInstance.removeEmailCampaign(eventId, linkId, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| Webcast event id | 
 **linkId** | **Number**| WebcastLink id | 
 **id** | **Number**|  | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="removeImageRendition"></a>
# **removeImageRendition**
> ImageContent removeImageRendition(eventId, id, alias)

Delete ImageContent rendition

Deletes an ImageContent rendition 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | WebcastEvent the Image is linked with.

var id = 56; // Number | Content id.

var alias = "alias_example"; // String | Image rendition alias.

apiInstance.removeImageRendition(eventId, id, alias).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| WebcastEvent the Image is linked with. | 
 **id** | **Number**| Content id. | 
 **alias** | **String**| Image rendition alias. | 

### Return type

[**ImageContent**](ImageContent.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="removeLink"></a>
# **removeLink**
> InlineResponse2002 removeLink(eventId, id)

Delete WebcastLink

Deletes a WebcastLink 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | 

var id = 56; // Number | 

apiInstance.removeLink(eventId, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**|  | 
 **id** | **Number**|  | 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="removeOnDemand"></a>
# **removeOnDemand**
> InlineResponse2001 removeOnDemand(eventId, id)

Remove OnDemand

Remove an OnDemand resource 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | WebcastEvent Id.

var id = 56; // Number | OnDemand id

apiInstance.removeOnDemand(eventId, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| WebcastEvent Id. | 
 **id** | **Number**| OnDemand id | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="removeRegistrationField"></a>
# **removeRegistrationField**
> InlineResponse2002 removeRegistrationField(eventId, linkId, id)

Delete RegistrationField

Deletes a RegistrationField 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | 

var linkId = 56; // Number | 

var id = 56; // Number | 

apiInstance.removeRegistrationField(eventId, linkId, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**|  | 
 **linkId** | **Number**|  | 
 **id** | **Number**|  | 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="removeRegistrationFieldTranslation"></a>
# **removeRegistrationFieldTranslation**
> InlineResponse2002 removeRegistrationFieldTranslation(eventId, linkId, fieldId, id)

Delete RegistrationFieldTranslation

Deletes a RegistrationFieldTranslation 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | 

var linkId = 56; // Number | 

var fieldId = 56; // Number | 

var id = 56; // Number | 

apiInstance.removeRegistrationFieldTranslation(eventId, linkId, fieldId, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**|  | 
 **linkId** | **Number**|  | 
 **fieldId** | **Number**|  | 
 **id** | **Number**|  | 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="removeSlideshowSource"></a>
# **removeSlideshowSource**
> SlideshowContent removeSlideshowSource(eventId, id, alias)

Delete SlideshowContent rendition

Deletes a SlideshowContent source 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | WebcastEvent the Image is linked with.

var id = 56; // Number | Content id.

var alias = "alias_example"; // String | Slideshow source alias.

apiInstance.removeSlideshowSource(eventId, id, alias).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| WebcastEvent the Image is linked with. | 
 **id** | **Number**| Content id. | 
 **alias** | **String**| Slideshow source alias. | 

### Return type

[**SlideshowContent**](SlideshowContent.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="removeWebcastAssetRendition"></a>
# **removeWebcastAssetRendition**
> WebcastLinkAsset removeWebcastAssetRendition(eventId, linkId, id, renditionName, body)

Deletes a WebcastLinkAsset rendition

Removes a WebcastLinkAsset rendition 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | WebcastEvent to which the link belongs.

var linkId = 56; // Number | Id of the WebcastLink where the assets belong.

var id = 56; // Number | Id of the asset where the rendition belongs

var renditionName = "renditionName_example"; // String | Name of the rendition to replace

var body = new webcastudio.WebcastLinkAsset(); // WebcastLinkAsset | 

apiInstance.removeWebcastAssetRendition(eventId, linkId, id, renditionName, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| WebcastEvent to which the link belongs. | 
 **linkId** | **Number**| Id of the WebcastLink where the assets belong. | 
 **id** | **Number**| Id of the asset where the rendition belongs | 
 **renditionName** | **String**| Name of the rendition to replace | 
 **body** | [**WebcastLinkAsset**](WebcastLinkAsset.md)|  | 

### Return type

[**WebcastLinkAsset**](WebcastLinkAsset.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="removeWebcastLinkAsset"></a>
# **removeWebcastLinkAsset**
> InlineResponse2001 removeWebcastLinkAsset(eventId, linkId, id)

Remove WebcastLinkAsset

Deletes a WebcastLinkAsset 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | Webcast Event id.

var linkId = 56; // Number | Webcast Link id.

var id = 56; // Number | 

apiInstance.removeWebcastLinkAsset(eventId, linkId, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| Webcast Event id. | 
 **linkId** | **Number**| Webcast Link id. | 
 **id** | **Number**|  | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="replaceImage"></a>
# **replaceImage**
> ImageContent replaceImage(eventId, id, component, content)

Replace ImageContent

Replaces an existing ImageContent. 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | WebcastEvent the Image is linked with.

var id = 56; // Number | Content id.

var component = "image"; // String | Webcast Component

var content = "/path/to/file.txt"; // File | File to upload.

apiInstance.replaceImage(eventId, id, component, content).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| WebcastEvent the Image is linked with. | 
 **id** | **Number**| Content id. | 
 **component** | **String**| Webcast Component | [default to image]
 **content** | **File**| File to upload. | 

### Return type

[**ImageContent**](ImageContent.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="replaceWebcastAsset"></a>
# **replaceWebcastAsset**
> WebcastLinkAsset replaceWebcastAsset(eventId, linkId, id, body)

Replaces a WebcastLinkAsset

Replaces a new WebcastLinkAsset 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | WebcastEvent to which the link belongs.

var linkId = 56; // Number | Id of the WebcastLink where the assets belong.

var id = 56; // Number | Id of the asset that you want to replace

var body = new webcastudio.WebcastLinkAsset(); // WebcastLinkAsset | 

apiInstance.replaceWebcastAsset(eventId, linkId, id, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| WebcastEvent to which the link belongs. | 
 **linkId** | **Number**| Id of the WebcastLink where the assets belong. | 
 **id** | **Number**| Id of the asset that you want to replace | 
 **body** | [**WebcastLinkAsset**](WebcastLinkAsset.md)|  | 

### Return type

[**WebcastLinkAsset**](WebcastLinkAsset.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="revokeWebcastEventMember"></a>
# **revokeWebcastEventMember**
> InlineResponse2003 revokeWebcastEventMember(eventId, userId)

Revoke WebcastEvent member privileges

Revoke WebcastEvent member privileges

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | 

var userId = 56; // Number | 

apiInstance.revokeWebcastEventMember(eventId, userId).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**|  | 
 **userId** | **Number**|  | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="terminateStreamingEngine"></a>
# **terminateStreamingEngine**
> StreamingEngine terminateStreamingEngine(eventId, id)

Terminate an Streaming Engine

Returns the affected StreamingEngine 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | 

var id = "id_example"; // String | 

apiInstance.terminateStreamingEngine(eventId, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**|  | 
 **id** | **String**|  | 

### Return type

[**StreamingEngine**](StreamingEngine.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateEmailCampaign"></a>
# **updateEmailCampaign**
> EmailCampaign updateEmailCampaign(eventId, linkId, id, body)

Update EmailCampaign

Update a EmailCampaign 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | Webcast event id

var linkId = 56; // Number | WebcastLink id

var id = 56; // Number | 

var body = new webcastudio.EmailCampaign(); // EmailCampaign | 

apiInstance.updateEmailCampaign(eventId, linkId, id, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| Webcast event id | 
 **linkId** | **Number**| WebcastLink id | 
 **id** | **Number**|  | 
 **body** | [**EmailCampaign**](EmailCampaign.md)|  | 

### Return type

[**EmailCampaign**](EmailCampaign.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateLink"></a>
# **updateLink**
> WebcastLinkItem updateLink(eventId, id, body)

Update WebcastLink

Update a WebcastLink 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | 

var id = 56; // Number | 

var body = new webcastudio.WebcastLinkItem(); // WebcastLinkItem | 

apiInstance.updateLink(eventId, id, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**|  | 
 **id** | **Number**|  | 
 **body** | [**WebcastLinkItem**](WebcastLinkItem.md)|  | 

### Return type

[**WebcastLinkItem**](WebcastLinkItem.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateLinkSettings"></a>
# **updateLinkSettings**
> WebcastLink updateLinkSettings(eventId, id, property)

Update WebcastLink settings

Update Webcast Link Settings

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | WebcastEvent id

var id = 56; // Number | WebcastLink id

var property = null; // Object | Settings property

apiInstance.updateLinkSettings(eventId, id, property).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| WebcastEvent id | 
 **id** | **Number**| WebcastLink id | 
 **property** | **Object**| Settings property | 

### Return type

[**WebcastLink**](WebcastLink.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

<a name="updateLinkSettingsLevel1"></a>
# **updateLinkSettingsLevel1**
> WebcastLink updateLinkSettingsLevel1(eventId, id, key1, property)

Update WebcastLink settings

Update Webcast Link Settings

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | WebcastEvent id

var id = 56; // Number | WebcastLink id

var key1 = "key1_example"; // String | Level 1 key

var property = null; // Object | Settings property

apiInstance.updateLinkSettingsLevel1(eventId, id, key1, property).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| WebcastEvent id | 
 **id** | **Number**| WebcastLink id | 
 **key1** | **String**| Level 1 key | 
 **property** | **Object**| Settings property | 

### Return type

[**WebcastLink**](WebcastLink.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

<a name="updateLinkSettingsLevel2"></a>
# **updateLinkSettingsLevel2**
> WebcastLink updateLinkSettingsLevel2(eventId, id, key1, key2, property)

Update WebcastLink settings

Update Webcast Link Settings

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | WebcastEvent id

var id = 56; // Number | WebcastLink id

var key1 = "key1_example"; // String | Level 1 key

var key2 = "key2_example"; // String | Level 2 key

var property = null; // Object | Settings property

apiInstance.updateLinkSettingsLevel2(eventId, id, key1, key2, property).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| WebcastEvent id | 
 **id** | **Number**| WebcastLink id | 
 **key1** | **String**| Level 1 key | 
 **key2** | **String**| Level 2 key | 
 **property** | **Object**| Settings property | 

### Return type

[**WebcastLink**](WebcastLink.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

<a name="updateLinkSettingsLevel3"></a>
# **updateLinkSettingsLevel3**
> WebcastLink updateLinkSettingsLevel3(eventId, id, key1, key2, key3, property)

Update WebcastLink settings

Update Webcast Link Settings

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | WebcastEvent id

var id = 56; // Number | WebcastLink id

var key1 = "key1_example"; // String | Level 1 key

var key2 = "key2_example"; // String | Level 2 key

var key3 = "key3_example"; // String | Level 3 key

var property = null; // Object | Settings property

apiInstance.updateLinkSettingsLevel3(eventId, id, key1, key2, key3, property).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| WebcastEvent id | 
 **id** | **Number**| WebcastLink id | 
 **key1** | **String**| Level 1 key | 
 **key2** | **String**| Level 2 key | 
 **key3** | **String**| Level 3 key | 
 **property** | **Object**| Settings property | 

### Return type

[**WebcastLink**](WebcastLink.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json, text/plain
 - **Accept**: application/json

<a name="updateOnDemand"></a>
# **updateOnDemand**
> OnDemand updateOnDemand(eventId, id, body)

Update OnDemand

Update an OnDemand resource 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | WebcastEvent id.

var id = 56; // Number | 

var body = new webcastudio.OnDemand(); // OnDemand | 

apiInstance.updateOnDemand(eventId, id, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| WebcastEvent id. | 
 **id** | **Number**|  | 
 **body** | [**OnDemand**](OnDemand.md)|  | 

### Return type

[**OnDemand**](OnDemand.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateRegistrationField"></a>
# **updateRegistrationField**
> RegistrationField updateRegistrationField(eventId, linkId, id, body)

Update RegistrationField

Update a WebcastLink 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | 

var linkId = 56; // Number | 

var id = 56; // Number | 

var body = new webcastudio.RegistrationField(); // RegistrationField | 

apiInstance.updateRegistrationField(eventId, linkId, id, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**|  | 
 **linkId** | **Number**|  | 
 **id** | **Number**|  | 
 **body** | [**RegistrationField**](RegistrationField.md)|  | 

### Return type

[**RegistrationField**](RegistrationField.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateRegistrationFieldTranslation"></a>
# **updateRegistrationFieldTranslation**
> RegistrationFieldTranslation updateRegistrationFieldTranslation(eventId, linkId, fieldId, id, body)

Update RegistrationFieldTranslation

Update a RegistrationFieldTranslation 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | 

var linkId = 56; // Number | 

var fieldId = 56; // Number | 

var id = 56; // Number | 

var body = new webcastudio.RegistrationFieldTranslation(); // RegistrationFieldTranslation | 

apiInstance.updateRegistrationFieldTranslation(eventId, linkId, fieldId, id, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**|  | 
 **linkId** | **Number**|  | 
 **fieldId** | **Number**|  | 
 **id** | **Number**|  | 
 **body** | [**RegistrationFieldTranslation**](RegistrationFieldTranslation.md)|  | 

### Return type

[**RegistrationFieldTranslation**](RegistrationFieldTranslation.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateTemplate"></a>
# **updateTemplate**
> WebcastTemplateInstance updateTemplate(eventId)

Update template instance files

Updates the WebcastTemplateInstance files 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | 

apiInstance.updateTemplate(eventId).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**|  | 

### Return type

[**WebcastTemplateInstance**](WebcastTemplateInstance.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="uploadImageRendition"></a>
# **uploadImageRendition**
> ImageContent uploadImageRendition(eventId, id, alias, rendition)

Upload ImageContent rendition

Upload an ImageContent rendition. 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | WebcastEvent the Image is linked with.

var id = 56; // Number | Content id.

var alias = "alias_example"; // String | Rendition alias.

var rendition = "/path/to/file.txt"; // File | File to upload.

apiInstance.uploadImageRendition(eventId, id, alias, rendition).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| WebcastEvent the Image is linked with. | 
 **id** | **Number**| Content id. | 
 **alias** | **String**| Rendition alias. | 
 **rendition** | **File**| File to upload. | 

### Return type

[**ImageContent**](ImageContent.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="uploadOndemandCuepoints"></a>
# **uploadOndemandCuepoints**
> OnDemand uploadOndemandCuepoints(eventId, id, cuepoints)

Replace current cuepoints

Upload a new cuepoints file replacing the current

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | Webcast Event id

var id = 56; // Number | OnDemand id

var cuepoints = "/path/to/file.txt"; // File | Cuepoints JSON file

apiInstance.uploadOndemandCuepoints(eventId, id, cuepoints).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| Webcast Event id | 
 **id** | **Number**| OnDemand id | 
 **cuepoints** | **File**| Cuepoints JSON file | 

### Return type

[**OnDemand**](OnDemand.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="uploadSlideshow"></a>
# **uploadSlideshow**
> SlideshowContent uploadSlideshow(eventId, sessionId, languageId, component, slideshow)

Upload SlideshowContent

Uploads a new SlideshowContent 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | WebcastEvent to link the Slideshow with.

var sessionId = 56; // Number | WebcastSession to link the Slideshow with.

var languageId = 56; // Number | WebcastLanguage to link the Slideshow with.

var component = "slideshow"; // String | Webcast Component

var slideshow = "/path/to/file.txt"; // File | File to upload.

apiInstance.uploadSlideshow(eventId, sessionId, languageId, component, slideshow).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| WebcastEvent to link the Slideshow with. | 
 **sessionId** | **Number**| WebcastSession to link the Slideshow with. | 
 **languageId** | **Number**| WebcastLanguage to link the Slideshow with. | 
 **component** | **String**| Webcast Component | [default to slideshow]
 **slideshow** | **File**| File to upload. | 

### Return type

[**SlideshowContent**](SlideshowContent.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="uploadSlideshowSource"></a>
# **uploadSlideshowSource**
> SlideshowContent uploadSlideshowSource(eventId, id, alias, source)

Upload SlideshowContent source

Upload a SlideshowContent source. 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.WebcastEventsApi();

var eventId = 56; // Number | WebcastEvent the Image is linked with.

var id = 56; // Number | Content id.

var alias = "alias_example"; // String | Source alias.

var source = "/path/to/file.txt"; // File | File to upload.

apiInstance.uploadSlideshowSource(eventId, id, alias, source).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**| WebcastEvent the Image is linked with. | 
 **id** | **Number**| Content id. | 
 **alias** | **String**| Source alias. | 
 **source** | **File**| File to upload. | 

### Return type

[**SlideshowContent**](SlideshowContent.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

