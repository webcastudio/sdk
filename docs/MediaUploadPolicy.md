# webcastudio.MediaUploadPolicy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** | URL where the media must be uploaded using multi-part encoding type. | [optional] 
**fields** | **Object** | Fields that should be added to the upload request | [optional] 


