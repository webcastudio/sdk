# webcastudio.WebcastSession

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**name** | **String** | Session name. | [optional] 
**description** | **String** | Session description. | [optional] 
**startingDate** | **String** | When Session starts. | [optional] 
**finishingDate** | **Date** | When Session finishes. | [optional] 
**endUserDestination** | **String** | Which application should handle link. | [optional] [default to &#39;lobby&#39;]


<a name="EndUserDestinationEnum"></a>
## Enum: EndUserDestinationEnum


* `webcast` (value: `"webcast"`)

* `lobby` (value: `"lobby"`)




