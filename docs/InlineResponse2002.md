# webcastudio.InlineResponse2002

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **String** |  | [optional] [default to &#39;ok&#39;]
**status** | **Number** |  | [optional] 


<a name="MessageEnum"></a>
## Enum: MessageEnum


* `ok` (value: `"ok"`)




