# webcastudio.WorkspaceJoinInvitationCreateBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** | email of the invited user | [optional] 
**aclRoleId** | **Number** | id of the user&#39;s role in the workspace. | [optional] 
**acceptPagePath** | **String** | URL path where the user can accept the invitation. | [optional] 


