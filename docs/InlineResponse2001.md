# webcastudio.InlineResponse2001

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **String** |  | [optional] [default to &#39;Ok&#39;]
**status** | **Number** |  | [optional] 


<a name="MessageEnum"></a>
## Enum: MessageEnum


* `Ok` (value: `"Ok"`)




