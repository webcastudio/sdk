# webcastudio.TranscodingPreset

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**name** | **String** | [[Description]]. | [optional] 
**description** | **String** | [[Description]]. | [optional] 
**vodPresetIds** | **String** | [[Description]]. | [optional] 
**livePresetId** | **String** | [[Description]]. | [optional] 
**enabled** | **Boolean** | [[Description]]. | [optional] 
**_public** | **Boolean** | [[Description]]. | [optional] 


