# webcastudio.SlideshowContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**name** | **String** | Slideshow description. | [optional] 
**type** | **String** | Content type. | [optional] [default to &#39;slideshow&#39;]
**meta** | [**SlideshowContentMeta**](SlideshowContentMeta.md) |  | [optional] 
**links** | [**[ContentLink]**](ContentLink.md) | Content links list. | [optional] 


<a name="TypeEnum"></a>
## Enum: TypeEnum


* `slideshow` (value: `"slideshow"`)




