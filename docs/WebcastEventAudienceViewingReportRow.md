# webcastudio.WebcastEventAudienceViewingReportRow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sid** | **String** | Access session id. | [optional] 
**eventId** | **Number** | Webcast Event id. | [optional] 
**linkToken** | **String** | Access Link token. | [optional] 
**languageId** | **Number** | Webcast Language id. | [optional] 
**language** | **String** | ISO 639-1 language code. | [optional] 
**firstSeen** | **Number** | First seen timestamp. | [optional] 
**lastSeen** | **Number** | Last seen timestamp. | [optional] 
**activeTime** | **Number** | Time active. | [optional] 
**deviceType** | **String** | Type of device used. | [optional] 
**browser** | **String** | Browser used. | [optional] 
**browserVersion** | **String** | Version of the browser used. | [optional] 
**platform** | **String** | Platform used. | [optional] 
**userId** | **Number** | User id if available. | [optional] 
**email** | **String** | User email if available. | [optional] 
**firstName** | **String** | User first name if available. | [optional] 
**lastName** | **String** | User last name if available. | [optional] 


