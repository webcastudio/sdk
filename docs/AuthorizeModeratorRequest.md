# webcastudio.AuthorizeModeratorRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **String** | Moderator token to be authorized | [optional] 


