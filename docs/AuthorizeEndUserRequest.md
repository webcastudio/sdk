# webcastudio.AuthorizeEndUserRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **String** | Token to check authorization. | [optional] 


