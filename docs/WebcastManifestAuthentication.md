# webcastudio.WebcastManifestAuthentication

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** | Authentication url. | [optional] 


