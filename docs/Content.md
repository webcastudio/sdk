# webcastudio.Content

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**name** | **String** | Content description. | [optional] 
**type** | **String** | Content type. | [optional] 
**meta** | **Object** | Content metadata. | [optional] 
**links** | [**[ContentLink]**](ContentLink.md) | Content links list. | [optional] 


<a name="TypeEnum"></a>
## Enum: TypeEnum


* `image` (value: `"image"`)

* `slideshow` (value: `"slideshow"`)




