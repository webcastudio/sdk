# webcastudio.StreamTarget

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**type** | **String** | Stream target type. | [optional] 
**playbackUrl** | **String** | Stream playback URL. | [optional] 
**settings** | **Object** | Settings object. | [optional] 
**weight** | **Number** | In which order the stream target should be provided to the player. | [optional] 
**streamTargetId** | **Number** | Stream target id in WSS. | [optional] 
**streamingAppInstanceId** | **Number** | Streaming App Instance id. | [optional] 


<a name="TypeEnum"></a>
## Enum: TypeEnum


* `cdn` (value: `"cdn"`)

* `ecdn` (value: `"ecdn"`)




