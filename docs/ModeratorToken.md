# webcastudio.ModeratorToken

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**token** | **String** | [[Description]]. | [optional] 
**expiresAt** | **Date** | [[Description]]. | [optional] 
**sessionId** | **Number** | Webcast session identifier | [optional] 
**endUserId** | **Number** | Moderator&#39;s endUser model identifier. | [optional] 
**moderator** | [**EndUser**](EndUser.md) |  | [optional] 


