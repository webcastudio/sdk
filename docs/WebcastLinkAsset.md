# webcastudio.WebcastLinkAsset

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**component** | **String** | [[Description]]. | [optional] 
**cuepointId** | **Number** | [[Description]]. | [optional] 


