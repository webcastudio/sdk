# webcastudio.Question

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Question identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**body** | **String** | Question text. | [optional] 
**tags** | **Number** | Question tags. | [optional] 
**endUserId** | **Number** | Identifier of the user who created this question. | [optional] 
**endUser** | [**EndUser**](EndUser.md) |  | [optional] 
**answers** | [**[Answer]**](Answer.md) |  | [optional] 
**assignedModerators** | [**[EndUser]**](EndUser.md) |  | [optional] 


