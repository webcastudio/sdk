# webcastudio.RegistrationGateToken

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**token** | **String** | [[Description]]. | [optional] 
**emailValidated** | **Boolean** | [[Description]]. | [optional] 
**consumed** | **Boolean** | [[Description]]. | [optional] 
**expiresAt** | **Date** | [[Description]]. | [optional] 


