# webcastudio.WebcastManifest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event** | [**WebcastManifestEvent**](WebcastManifestEvent.md) |  | [optional] 
**session** | [**WebcastSession**](WebcastSession.md) |  | [optional] 
**language** | [**WebcastLanguage**](WebcastLanguage.md) |  | [optional] 
**authentication** | [**WebcastManifestAuthentication**](WebcastManifestAuthentication.md) |  | [optional] 
**streamLiveService** | [**WebcastManifestStreamLiveService**](WebcastManifestStreamLiveService.md) |  | [optional] 
**streamOdService** | [**WebcastManifestStreamOdService**](WebcastManifestStreamOdService.md) |  | [optional] 
**contentsService** | [**WebcastManifestStreamLiveService**](WebcastManifestStreamLiveService.md) |  | [optional] 
**analytics** | [**WebcastManifestAnalytics**](WebcastManifestAnalytics.md) |  | [optional] 
**interaction** | [**WebcastManifestInteraction**](WebcastManifestInteraction.md) |  | [optional] 


