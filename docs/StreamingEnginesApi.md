# webcastudio.StreamingEnginesApi

All URIs are relative to *http://localhost:3000/api/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**terminateStreamingEngine**](StreamingEnginesApi.md#terminateStreamingEngine) | **DELETE** /events/{eventId}/streamingengines/{id} | Terminate an streaming engine


<a name="terminateStreamingEngine"></a>
# **terminateStreamingEngine**
> StreamingEngine terminateStreamingEngine(eventId, id)

Terminate an streaming engine

Returns the affected StreamingEngine 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.StreamingEnginesApi();

var eventId = 56; // Number | 

var id = "id_example"; // String | 

apiInstance.terminateStreamingEngine(eventId, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eventId** | **Number**|  | 
 **id** | **String**|  | 

### Return type

[**StreamingEngine**](StreamingEngine.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

