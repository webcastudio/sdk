# webcastudio.WebcastStream

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**src** | **String** | Stream source URL. | [optional] 
**type** | **String** | Source mimetype. | [optional] 
**streamType** | **String** | Stream type. | [optional] 
**cuepoints** | **[Object]** |  | [optional] 
**deliveryNetwork** | **String** | Stream delivery network. | [optional] 


<a name="StreamTypeEnum"></a>
## Enum: StreamTypeEnum


* `live` (value: `"live"`)

* `ondemand` (value: `"ondemand"`)




