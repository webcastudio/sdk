# webcastudio.AclRole

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**name** | **String** | Role name. | [optional] 
**description** | **String** | Role description. | [optional] 
**resource** | **String** | Which resource this role refers to. | [optional] 
**permissions** | **Number** | Permissions mask. | [optional] 


<a name="ResourceEnum"></a>
## Enum: ResourceEnum


* `Account` (value: `"Account"`)

* `Workspace` (value: `"Workspace"`)

* `WebcastEvent` (value: `"WebcastEvent"`)




