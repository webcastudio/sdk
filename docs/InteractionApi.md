# webcastudio.InteractionApi

All URIs are relative to *https://api.vancastvideo.com/api/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**assignModerator**](InteractionApi.md#assignModerator) | **POST** /interaction/qa/{sessionId}/questions/{id}/assign/{endUserId} | Assign a moderator to a question
[**authorizeModerator**](InteractionApi.md#authorizeModerator) | **POST** /interaction/qa/{sessionId}/authorize | Check a moderator authorization
[**createAnswer**](InteractionApi.md#createAnswer) | **POST** /interaction/qa/{sessionId}/questions/{id}/answers | Create Answer
[**createModerationToken**](InteractionApi.md#createModerationToken) | **POST** /interaction/qa/{sessionId}/auth | Create a Moderation token
[**createQuestion**](InteractionApi.md#createQuestion) | **POST** /interaction/qa/{sessionId}/questions | Create Question
[**findAllQuestions**](InteractionApi.md#findAllQuestions) | **GET** /interaction/qa/{sessionId}/questions | Find Question
[**removeQuestions**](InteractionApi.md#removeQuestions) | **DELETE** /interaction/qa/{sessionId}/questions | Removes all questions and answers from a session
[**unAssignModerator**](InteractionApi.md#unAssignModerator) | **DELETE** /interaction/qa/{sessionId}/questions/{id}/assign/{endUserId} | Removes a moderator from a question
[**updateQuestion**](InteractionApi.md#updateQuestion) | **PUT** /interaction/qa/{sessionId}/questions/{id} | Update Question


<a name="assignModerator"></a>
# **assignModerator**
> Question assignModerator(sessionId, id, endUserId, opts)

Assign a moderator to a question

Assigns a moderator to a question and returns affected question. 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.InteractionApi();

var sessionId = 56; // Number | 

var id = 56; // Number | 

var endUserId = 56; // Number | 

var opts = { 
  'publish': true // Boolean | When true, publishes a message to notify webcastudio-socket
};
apiInstance.assignModerator(sessionId, id, endUserId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionId** | **Number**|  | 
 **id** | **Number**|  | 
 **endUserId** | **Number**|  | 
 **publish** | **Boolean**| When true, publishes a message to notify webcastudio-socket | [optional] [default to true]

### Return type

[**Question**](Question.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="authorizeModerator"></a>
# **authorizeModerator**
> ModeratorToken authorizeModerator(sessionId, body)

Check a moderator authorization

Check moderator authorization 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.InteractionApi();

var sessionId = 56; // Number | 

var body = new webcastudio.AuthorizeModeratorRequest(); // AuthorizeModeratorRequest | 

apiInstance.authorizeModerator(sessionId, body).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionId** | **Number**|  | 
 **body** | [**AuthorizeModeratorRequest**](AuthorizeModeratorRequest.md)|  | 

### Return type

[**ModeratorToken**](ModeratorToken.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createAnswer"></a>
# **createAnswer**
> Answer createAnswer(sessionId, id, body, opts)

Create Answer

Creates a new Answer 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.InteractionApi();

var sessionId = 56; // Number | 

var id = 56; // Number | 

var body = new webcastudio.Answer(); // Answer | 

var opts = { 
  'publish': true // Boolean | When true, publishes a message to notify webcastudio-socket
};
apiInstance.createAnswer(sessionId, id, body, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionId** | **Number**|  | 
 **id** | **Number**|  | 
 **body** | [**Answer**](Answer.md)|  | 
 **publish** | **Boolean**| When true, publishes a message to notify webcastudio-socket | [optional] [default to true]

### Return type

[**Answer**](Answer.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createModerationToken"></a>
# **createModerationToken**
> ModeratorToken createModerationToken(sessionId)

Create a Moderation token

Requests a new Moderator token 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.InteractionApi();

var sessionId = 56; // Number | 

apiInstance.createModerationToken(sessionId).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionId** | **Number**|  | 

### Return type

[**ModeratorToken**](ModeratorToken.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createQuestion"></a>
# **createQuestion**
> Question createQuestion(sessionId, body, opts)

Create Question

Creates a new Question 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.InteractionApi();

var sessionId = 56; // Number | 

var body = new webcastudio.Question(); // Question | 

var opts = { 
  'publish': true // Boolean | When true, publishes a message to notify webcastudio-socket
};
apiInstance.createQuestion(sessionId, body, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionId** | **Number**|  | 
 **body** | [**Question**](Question.md)|  | 
 **publish** | **Boolean**| When true, publishes a message to notify webcastudio-socket | [optional] [default to true]

### Return type

[**Question**](Question.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="findAllQuestions"></a>
# **findAllQuestions**
> QuestionList findAllQuestions(sessionId, opts)

Find Question

Find all Question matching the query.

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.InteractionApi();

var sessionId = 56; // Number | 

var opts = { 
  'limit': 56, // Number | Limit of items per page
  'offset': 56, // Number | Items offset
  'where': "where_example" // String | Where statement
};
apiInstance.findAllQuestions(sessionId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionId** | **Number**|  | 
 **limit** | **Number**| Limit of items per page | [optional] 
 **offset** | **Number**| Items offset | [optional] 
 **where** | **String**| Where statement | [optional] 

### Return type

[**QuestionList**](QuestionList.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="removeQuestions"></a>
# **removeQuestions**
> removeQuestions(sessionId, opts)

Removes all questions and answers from a session

Removes all questions and answers from a session. 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.InteractionApi();

var sessionId = 56; // Number | 

var opts = { 
  'publish': true // Boolean | When true, publishes a message to notify webcastudio-socket
};
apiInstance.removeQuestions(sessionId, opts).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionId** | **Number**|  | 
 **publish** | **Boolean**| When true, publishes a message to notify webcastudio-socket | [optional] [default to true]

### Return type

null (empty response body)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="unAssignModerator"></a>
# **unAssignModerator**
> Question unAssignModerator(sessionId, id, endUserId, opts)

Removes a moderator from a question

Removes a moderator from a question and returns affected question. 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.InteractionApi();

var sessionId = 56; // Number | 

var id = 56; // Number | 

var endUserId = 56; // Number | 

var opts = { 
  'publish': true // Boolean | When true, publishes a message to notify webcastudio-socket
};
apiInstance.unAssignModerator(sessionId, id, endUserId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionId** | **Number**|  | 
 **id** | **Number**|  | 
 **endUserId** | **Number**|  | 
 **publish** | **Boolean**| When true, publishes a message to notify webcastudio-socket | [optional] [default to true]

### Return type

[**Question**](Question.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateQuestion"></a>
# **updateQuestion**
> Question updateQuestion(sessionId, id, body, opts)

Update Question

Update a Question 

### Example
```javascript
var webcastudio = require('@webcastudio/sdk');
var defaultClient = webcastudio.ApiClient.instance;

// Configure API key authorization: ApiKeyHeader
var ApiKeyHeader = defaultClient.authentications['ApiKeyHeader'];
ApiKeyHeader.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKeyHeader.apiKeyPrefix = 'Token';

var apiInstance = new webcastudio.InteractionApi();

var sessionId = 56; // Number | 

var id = 56; // Number | 

var body = new webcastudio.Question(); // Question | 

var opts = { 
  'publish': true // Boolean | When true, publishes a message to notify webcastudio-socket
};
apiInstance.updateQuestion(sessionId, id, body, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionId** | **Number**|  | 
 **id** | **Number**|  | 
 **body** | [**Question**](Question.md)|  | 
 **publish** | **Boolean**| When true, publishes a message to notify webcastudio-socket | [optional] [default to true]

### Return type

[**Question**](Question.md)

### Authorization

[ApiKeyHeader](../README.md#ApiKeyHeader)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

