# webcastudio.WebcastManifestEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**description** | **String** | Event description. | [optional] 
**name** | **String** | Event title. | [optional] 
**startingDate** | **Date** | When Event starts. | [optional] 
**finishingDate** | **Date** | When Event finishes. | [optional] 
**workspaceId** | **Number** | Workspace Id. | [optional] 
**status** | **String** | Event status. | [optional] 
**timezone** | **String** | Event timezone. | [optional] 
**mainLanguageId** | **Number** | Event main WebcastLanguage id. | [optional] 
**workspace** | [**Workspace**](Workspace.md) | Workspace. | [optional] 
**template** | [**WebcastTemplateInstance**](WebcastTemplateInstance.md) | WebcastTemplate instance. | [optional] 
**languages** | [**[WebcastLanguage]**](WebcastLanguage.md) | Event languages. | [optional] 


