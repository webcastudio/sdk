# webcastudio.WebcastLinkItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**description** | **String** | Link description. | [optional] 
**token** | **String** | Unique link non-numeric identifier. | [optional] 
**url** | **String** | Link url | [optional] 
**accessType** | **String** | Link access type. | [optional] 
**enabled** | **Boolean** | Whether the link is enabled. | [optional] [default to false]
**validUntil** | **String** | Date when the link should be disabled automatically. | [optional] 
**settings** | **Object** | Access settings. | [optional] 
**eventId** | **Number** | Webcast Event id. | [optional] 


<a name="AccessTypeEnum"></a>
## Enum: AccessTypeEnum


* `open` (value: `"open"`)

* `list` (value: `"list"`)

* `sso` (value: `"sso"`)

* `registration_gate` (value: `"registration_gate"`)

* `sso_email_campaign` (value: `"sso_email_campaign"`)




