# webcastudio.AddWebcastEventMemberRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | **Number** | User id | [optional] 
**aclRoleId** | **Number** | WebcastEvent ACL role id | [optional] 


