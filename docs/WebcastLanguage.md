# webcastudio.WebcastLanguage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**code** | **String** | Language locale code. | [optional] 
**description** | **String** | Language description. | [optional] 
**standard** | **String** | Language ISO standard. | [optional] [default to &#39;ISO 639-1&#39;]
**eventId** | **Number** | WebcastEvent Id. | [optional] 


<a name="StandardEnum"></a>
## Enum: StandardEnum


* `1` (value: `"ISO 639-1"`)




