# webcastudio.CopySessionAsVodRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**session** | [**CopySessionAsVodRequestSession**](CopySessionAsVodRequestSession.md) |  | [optional] 
**liveSessions** | **[Number]** | Live sessions to create the VODs from. | [optional] 
**clipStart** | **Number** | Clip start time | [optional] 
**clipDuration** | **Number** | Clip duration time | [optional] 


