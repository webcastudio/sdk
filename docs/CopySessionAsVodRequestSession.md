# webcastudio.CopySessionAsVodRequestSession

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | New Session name | [optional] 
**description** | **String** | New Session description | [optional] 
**startingDate** | **Date** | New Session starting date | [optional] 
**finishingDate** | **Date** | New Session finishing date | [optional] 


