# webcastudio.WorkspaceJoinInvitation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Identifier. | [optional] 
**createdAt** | **Date** | When record was created. | [optional] 
**updatedAt** | **Date** | Last time the record was updated. | [optional] 
**email** | **String** | User&#39;s email. | [optional] 
**suggestedFirstName** | **String** | Suggested name for the user. | [optional] 
**suggestedLastName** | **String** | Suggested user last name. | [optional] 
**authToken** | **String** | Authorization token string. | [optional] 
**expiresAt** | **Date** | Date when this token expires. | [optional] 
**consumed** | **Boolean** | Whether this token has been consumed successfully or not. | [optional] 
**failureAttempts** | **Number** | Join failure attempts with this token. | [optional] 


