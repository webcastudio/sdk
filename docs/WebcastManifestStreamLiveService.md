# webcastudio.WebcastManifestStreamLiveService

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**interval** | **Number** | Fetch interval. | [optional] 
**url** | **String** | Service url. | [optional] 


