# webcastudio.WebcastStreamsResourceList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**[WebcastStream]**](WebcastStream.md) |  | [optional] 
**totalItems** | **Number** | Total number of existing webcast streams. | [optional] 
**isTruncated** | **Boolean** | Whether the requests returns all items available. | [optional] 


