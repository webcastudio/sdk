# webcastudio.WebcastManifestInteractionRest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** | Rest service url. | [optional] 


