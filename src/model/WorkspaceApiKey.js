/**
 * Webcastudio API
 * Webcastudio WMS API
 *
 * OpenAPI spec version: 4.0.0-beta.21
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.webcastudio) {
      root.webcastudio = {};
    }
    root.webcastudio.WorkspaceApiKey = factory(root.webcastudio.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The WorkspaceApiKey model module.
   * @module model/WorkspaceApiKey
   * @version 4.0.0-beta.21
   */

  /**
   * Constructs a new <code>WorkspaceApiKey</code>.
   * @alias module:model/WorkspaceApiKey
   * @class
   */
  var exports = function() {
    var _this = this;











  };

  /**
   * Constructs a <code>WorkspaceApiKey</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/WorkspaceApiKey} obj Optional instance to populate.
   * @return {module:model/WorkspaceApiKey} The populated <code>WorkspaceApiKey</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Number');
      }
      if (data.hasOwnProperty('createdAt')) {
        obj['createdAt'] = ApiClient.convertToType(data['createdAt'], 'Date');
      }
      if (data.hasOwnProperty('updatedAt')) {
        obj['updatedAt'] = ApiClient.convertToType(data['updatedAt'], 'Date');
      }
      if (data.hasOwnProperty('token')) {
        obj['token'] = ApiClient.convertToType(data['token'], 'String');
      }
      if (data.hasOwnProperty('enabled')) {
        obj['enabled'] = ApiClient.convertToType(data['enabled'], 'Boolean');
      }
      if (data.hasOwnProperty('description')) {
        obj['description'] = ApiClient.convertToType(data['description'], 'String');
      }
      if (data.hasOwnProperty('workspacePermissions')) {
        obj['workspacePermissions'] = ApiClient.convertToType(data['workspacePermissions'], 'Number');
      }
      if (data.hasOwnProperty('webcastEventPermissions')) {
        obj['webcastEventPermissions'] = ApiClient.convertToType(data['webcastEventPermissions'], 'Number');
      }
      if (data.hasOwnProperty('workspaceId')) {
        obj['workspaceId'] = ApiClient.convertToType(data['workspaceId'], 'Number');
      }
      if (data.hasOwnProperty('createdBy')) {
        obj['createdBy'] = ApiClient.convertToType(data['createdBy'], 'Number');
      }
    }
    return obj;
  };

  /**
   * Identifier.
   * @member {Number} id
   */
  exports.prototype['id'] = undefined;
  /**
   * When record was created.
   * @member {Date} createdAt
   */
  exports.prototype['createdAt'] = undefined;
  /**
   * Last time the record was updated.
   * @member {Date} updatedAt
   */
  exports.prototype['updatedAt'] = undefined;
  /**
   * Auth token to access the API.
   * @member {String} token
   */
  exports.prototype['token'] = undefined;
  /**
   * Whether the api key is enabled.
   * @member {Boolean} enabled
   */
  exports.prototype['enabled'] = undefined;
  /**
   * Short description to identify the api key.
   * @member {String} description
   */
  exports.prototype['description'] = undefined;
  /**
   * Permissions of this api key over its workspace.
   * @member {Number} workspacePermissions
   */
  exports.prototype['workspacePermissions'] = undefined;
  /**
   * Permissions of this api key over the events of the workspace.
   * @member {Number} webcastEventPermissions
   */
  exports.prototype['webcastEventPermissions'] = undefined;
  /**
   * Id of the workspace where the api key belongs.
   * @member {Number} workspaceId
   */
  exports.prototype['workspaceId'] = undefined;
  /**
   * Id of the user who created the api key.
   * @member {Number} createdBy
   */
  exports.prototype['createdBy'] = undefined;



  return exports;
}));


