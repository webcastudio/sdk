/**
 * Webcastudio API
 * Webcastudio WMS API
 *
 * OpenAPI spec version: 4.0.0-beta.21
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/WebcastLanguage', 'model/WebcastSession'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./WebcastLanguage'), require('./WebcastSession'));
  } else {
    // Browser globals (root is window)
    if (!root.webcastudio) {
      root.webcastudio = {};
    }
    root.webcastudio.WebcastEvent = factory(root.webcastudio.ApiClient, root.webcastudio.WebcastLanguage, root.webcastudio.WebcastSession);
  }
}(this, function(ApiClient, WebcastLanguage, WebcastSession) {
  'use strict';




  /**
   * The WebcastEvent model module.
   * @module model/WebcastEvent
   * @version 4.0.0-beta.21
   */

  /**
   * Constructs a new <code>WebcastEvent</code>.
   * @alias module:model/WebcastEvent
   * @class
   */
  var exports = function() {
    var _this = this;














  };

  /**
   * Constructs a <code>WebcastEvent</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/WebcastEvent} obj Optional instance to populate.
   * @return {module:model/WebcastEvent} The populated <code>WebcastEvent</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Number');
      }
      if (data.hasOwnProperty('createdAt')) {
        obj['createdAt'] = ApiClient.convertToType(data['createdAt'], 'Date');
      }
      if (data.hasOwnProperty('updatedAt')) {
        obj['updatedAt'] = ApiClient.convertToType(data['updatedAt'], 'Date');
      }
      if (data.hasOwnProperty('description')) {
        obj['description'] = ApiClient.convertToType(data['description'], 'String');
      }
      if (data.hasOwnProperty('name')) {
        obj['name'] = ApiClient.convertToType(data['name'], 'String');
      }
      if (data.hasOwnProperty('startingDate')) {
        obj['startingDate'] = ApiClient.convertToType(data['startingDate'], 'Date');
      }
      if (data.hasOwnProperty('finishingDate')) {
        obj['finishingDate'] = ApiClient.convertToType(data['finishingDate'], 'Date');
      }
      if (data.hasOwnProperty('workspaceId')) {
        obj['workspaceId'] = ApiClient.convertToType(data['workspaceId'], 'Number');
      }
      if (data.hasOwnProperty('status')) {
        obj['status'] = ApiClient.convertToType(data['status'], 'String');
      }
      if (data.hasOwnProperty('timezone')) {
        obj['timezone'] = ApiClient.convertToType(data['timezone'], 'String');
      }
      if (data.hasOwnProperty('mainLanguageId')) {
        obj['mainLanguageId'] = ApiClient.convertToType(data['mainLanguageId'], 'Number');
      }
      if (data.hasOwnProperty('languages')) {
        obj['languages'] = ApiClient.convertToType(data['languages'], [WebcastLanguage]);
      }
      if (data.hasOwnProperty('sessions')) {
        obj['sessions'] = ApiClient.convertToType(data['sessions'], [WebcastSession]);
      }
    }
    return obj;
  };

  /**
   * Identifier.
   * @member {Number} id
   */
  exports.prototype['id'] = undefined;
  /**
   * When record was created.
   * @member {Date} createdAt
   */
  exports.prototype['createdAt'] = undefined;
  /**
   * Last time the record was updated.
   * @member {Date} updatedAt
   */
  exports.prototype['updatedAt'] = undefined;
  /**
   * Event description.
   * @member {String} description
   */
  exports.prototype['description'] = undefined;
  /**
   * Event title.
   * @member {String} name
   */
  exports.prototype['name'] = undefined;
  /**
   * When Event starts.
   * @member {Date} startingDate
   */
  exports.prototype['startingDate'] = undefined;
  /**
   * When Event finishes.
   * @member {Date} finishingDate
   */
  exports.prototype['finishingDate'] = undefined;
  /**
   * Workspace Id.
   * @member {Number} workspaceId
   */
  exports.prototype['workspaceId'] = undefined;
  /**
   * Event status.
   * @member {String} status
   */
  exports.prototype['status'] = undefined;
  /**
   * Event timezone.
   * @member {String} timezone
   */
  exports.prototype['timezone'] = undefined;
  /**
   * Event main WebcastLanguage id.
   * @member {Number} mainLanguageId
   */
  exports.prototype['mainLanguageId'] = undefined;
  /**
   * Event languages.
   * @member {Array.<module:model/WebcastLanguage>} languages
   */
  exports.prototype['languages'] = undefined;
  /**
   * Event sessions.
   * @member {Array.<module:model/WebcastSession>} sessions
   */
  exports.prototype['sessions'] = undefined;



  return exports;
}));


