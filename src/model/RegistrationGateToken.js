/**
 * Webcastudio API
 * Webcastudio WMS API
 *
 * OpenAPI spec version: 4.0.0-beta.21
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.webcastudio) {
      root.webcastudio = {};
    }
    root.webcastudio.RegistrationGateToken = factory(root.webcastudio.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The RegistrationGateToken model module.
   * @module model/RegistrationGateToken
   * @version 4.0.0-beta.21
   */

  /**
   * Constructs a new <code>RegistrationGateToken</code>.
   * @alias module:model/RegistrationGateToken
   * @class
   */
  var exports = function() {
    var _this = this;








  };

  /**
   * Constructs a <code>RegistrationGateToken</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/RegistrationGateToken} obj Optional instance to populate.
   * @return {module:model/RegistrationGateToken} The populated <code>RegistrationGateToken</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Number');
      }
      if (data.hasOwnProperty('createdAt')) {
        obj['createdAt'] = ApiClient.convertToType(data['createdAt'], 'Date');
      }
      if (data.hasOwnProperty('updatedAt')) {
        obj['updatedAt'] = ApiClient.convertToType(data['updatedAt'], 'Date');
      }
      if (data.hasOwnProperty('token')) {
        obj['token'] = ApiClient.convertToType(data['token'], 'String');
      }
      if (data.hasOwnProperty('emailValidated')) {
        obj['emailValidated'] = ApiClient.convertToType(data['emailValidated'], 'Boolean');
      }
      if (data.hasOwnProperty('consumed')) {
        obj['consumed'] = ApiClient.convertToType(data['consumed'], 'Boolean');
      }
      if (data.hasOwnProperty('expiresAt')) {
        obj['expiresAt'] = ApiClient.convertToType(data['expiresAt'], 'Date');
      }
    }
    return obj;
  };

  /**
   * Identifier.
   * @member {Number} id
   */
  exports.prototype['id'] = undefined;
  /**
   * When record was created.
   * @member {Date} createdAt
   */
  exports.prototype['createdAt'] = undefined;
  /**
   * Last time the record was updated.
   * @member {Date} updatedAt
   */
  exports.prototype['updatedAt'] = undefined;
  /**
   * [[Description]].
   * @member {String} token
   */
  exports.prototype['token'] = undefined;
  /**
   * [[Description]].
   * @member {Boolean} emailValidated
   */
  exports.prototype['emailValidated'] = undefined;
  /**
   * [[Description]].
   * @member {Boolean} consumed
   */
  exports.prototype['consumed'] = undefined;
  /**
   * [[Description]].
   * @member {Date} expiresAt
   */
  exports.prototype['expiresAt'] = undefined;



  return exports;
}));


