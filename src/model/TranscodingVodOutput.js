/**
 * Webcastudio API
 * Webcastudio WMS API
 *
 * OpenAPI spec version: 4.0.0-beta.21
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.webcastudio) {
      root.webcastudio = {};
    }
    root.webcastudio.TranscodingVodOutput = factory(root.webcastudio.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The TranscodingVodOutput model module.
   * @module model/TranscodingVodOutput
   * @version 4.0.0-beta.21
   */

  /**
   * Constructs a new <code>TranscodingVodOutput</code>.
   * @alias module:model/TranscodingVodOutput
   * @class
   */
  var exports = function() {
    var _this = this;








  };

  /**
   * Constructs a <code>TranscodingVodOutput</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/TranscodingVodOutput} obj Optional instance to populate.
   * @return {module:model/TranscodingVodOutput} The populated <code>TranscodingVodOutput</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Number');
      }
      if (data.hasOwnProperty('createdAt')) {
        obj['createdAt'] = ApiClient.convertToType(data['createdAt'], 'Date');
      }
      if (data.hasOwnProperty('updatedAt')) {
        obj['updatedAt'] = ApiClient.convertToType(data['updatedAt'], 'Date');
      }
      if (data.hasOwnProperty('key')) {
        obj['key'] = ApiClient.convertToType(data['key'], 'String');
      }
      if (data.hasOwnProperty('segmentDuration')) {
        obj['segmentDuration'] = ApiClient.convertToType(data['segmentDuration'], 'Number');
      }
      if (data.hasOwnProperty('presetId')) {
        obj['presetId'] = ApiClient.convertToType(data['presetId'], 'String');
      }
      if (data.hasOwnProperty('thumbnailPattern')) {
        obj['thumbnailPattern'] = ApiClient.convertToType(data['thumbnailPattern'], 'String');
      }
    }
    return obj;
  };

  /**
   * Identifier.
   * @member {Number} id
   */
  exports.prototype['id'] = undefined;
  /**
   * When record was created.
   * @member {Date} createdAt
   */
  exports.prototype['createdAt'] = undefined;
  /**
   * Last time the record was updated.
   * @member {Date} updatedAt
   */
  exports.prototype['updatedAt'] = undefined;
  /**
   * [[Description]].
   * @member {String} key
   */
  exports.prototype['key'] = undefined;
  /**
   * [[Description]].
   * @member {Number} segmentDuration
   */
  exports.prototype['segmentDuration'] = undefined;
  /**
   * [[Description]].
   * @member {String} presetId
   */
  exports.prototype['presetId'] = undefined;
  /**
   * [[Description]].
   * @member {String} thumbnailPattern
   */
  exports.prototype['thumbnailPattern'] = undefined;



  return exports;
}));


