/**
 * Webcastudio API
 * Webcastudio WMS API
 *
 * OpenAPI spec version: 4.0.0-beta.21
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.webcastudio) {
      root.webcastudio = {};
    }
    root.webcastudio.MediaUploadPolicy = factory(root.webcastudio.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The MediaUploadPolicy model module.
   * @module model/MediaUploadPolicy
   * @version 4.0.0-beta.21
   */

  /**
   * Constructs a new <code>MediaUploadPolicy</code>.
   * Media upload policy
   * @alias module:model/MediaUploadPolicy
   * @class
   */
  var exports = function() {
    var _this = this;



  };

  /**
   * Constructs a <code>MediaUploadPolicy</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/MediaUploadPolicy} obj Optional instance to populate.
   * @return {module:model/MediaUploadPolicy} The populated <code>MediaUploadPolicy</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('url')) {
        obj['url'] = ApiClient.convertToType(data['url'], 'String');
      }
      if (data.hasOwnProperty('fields')) {
        obj['fields'] = ApiClient.convertToType(data['fields'], Object);
      }
    }
    return obj;
  };

  /**
   * URL where the media must be uploaded using multi-part encoding type.
   * @member {String} url
   */
  exports.prototype['url'] = undefined;
  /**
   * Fields that should be added to the upload request
   * @member {Object} fields
   */
  exports.prototype['fields'] = undefined;



  return exports;
}));


