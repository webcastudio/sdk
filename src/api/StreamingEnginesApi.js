/**
 * Webcastudio API
 * Webcastudio WMS API
 *
 * OpenAPI spec version: 4.0.0-beta.20
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.3.1
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/StreamingEngine'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/StreamingEngine'));
  } else {
    // Browser globals (root is window)
    if (!root.webcastudio) {
      root.webcastudio = {};
    }
    root.webcastudio.StreamingEnginesApi = factory(root.webcastudio.ApiClient, root.webcastudio.StreamingEngine);
  }
}(this, function(ApiClient, StreamingEngine) {
  'use strict';

  /**
   * StreamingEngines service.
   * @module api/StreamingEnginesApi
   * @version 4.0.0-beta.20
   */

  /**
   * Constructs a new StreamingEnginesApi. 
   * @alias module:api/StreamingEnginesApi
   * @class
   * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;



    /**
     * Terminate an streaming engine
     * Returns the affected StreamingEngine 
     * @param {Number} eventId 
     * @param {String} id 
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/StreamingEngine} and HTTP response
     */
    this.terminateStreamingEngineWithHttpInfo = function(eventId, id) {
      var postBody = null;

      // verify the required parameter 'eventId' is set
      if (eventId === undefined || eventId === null) {
        throw new Error("Missing the required parameter 'eventId' when calling terminateStreamingEngine");
      }

      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling terminateStreamingEngine");
      }


      var pathParams = {
        'eventId': eventId,
        'id': id
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['ApiKeyHeader'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = StreamingEngine;

      return this.apiClient.callApi(
        '/events/{eventId}/streamingengines/{id}', 'DELETE',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    };

    /**
     * Terminate an streaming engine
     * Returns the affected StreamingEngine 
     * @param {Number} eventId 
     * @param {String} id 
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/StreamingEngine}
     */
    this.terminateStreamingEngine = function(eventId, id) {
      return this.terminateStreamingEngineWithHttpInfo(eventId, id)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    };
  };

  return exports;
}));
